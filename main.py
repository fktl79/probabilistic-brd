from typing import List, Dict, Tuple
import pickle
import util.plotting
import util.parameter_tuning
import util.data_generation
from util.other import timing
import random
import time

def run_auction(setup_info: Dict[str, Dict], jobs_info: List[Dict], machines_info: List[Dict],
                weightings: Dict[str, float]) -> List[Dict]:
    from auction.job import Job
    from auction.machine import Machine
    from auction.auctioneer import Auctioneer

    jobs = {job.get('name'): Job(job.get('name'),
                                 job.get('due_date'),
                                 job.get('material'),
                                 job.get('valid_machines'),
                                 setup_info.get(job.get('material')),
                                 weightings)
            for job in jobs_info}

    machines = {machine.get('name'): Machine(machine.get('name'),
                                             machine.get('init_date'),
                                             machine.get('init_material'))
                for machine in machines_info}

    auctioneer = Auctioneer(machines, jobs)

    result = auctioneer.schedule_jobs()

    return result


def run_ga(setup_info: Dict[str, Dict], jobs_info: List[Dict], machines_info: List[Dict], popsize=100, prob_x=0.9, prob_mut=0.2, selection_operator = 'roulette', num_parents = 20, selection_tournament_size = 5, crossover_operator = 'singlepoint', num_cuts = 2, mutation_operator = 'random', replacement_operator = 'elitism', n_best = 3, replacement_tournament_size = 5) -> Tuple[List[float], List[dict]]:
    from ga.genetic_algorithm import SchedulingGA
    from ga.machine import Machine, Job

    genes = [j.get('name') for j in jobs_info]
    jobs = {j.get('name'): Job(j.get('name'),
                               j.get('due_date'),
                               j.get('material'),
                               j.get('valid_machines'))
            for j in jobs_info}
    machines = {machine.get('name'): Machine(machine.get('name'),
                machine.get('init_date'),
                machine.get('init_material'),
                setup_info)
                for machine in machines_info}

    ga = SchedulingGA(genes=genes, popsize=popsize, prob_x=prob_x, prob_mut=prob_mut, jobs=jobs, machines=machines, selection_operator=selection_operator, num_parents=num_parents, selection_tournament_size=selection_tournament_size, crossover_operator=crossover_operator, num_cuts=num_cuts, mutation_operator=mutation_operator, replacement_operator=replacement_operator, n_best=n_best, replacement_tournament_size=replacement_tournament_size)
    result = run_ga_with_timing(ga, jobs, machines)
    return ga.utilities, result

    # pareto = []
    # for idx, chromosome in enumerate(ga.population):
    #     result = ga_build_phenotype(chromosome, jobs, machines)
    #
    #     sum_tardiness = 0
    #     sum_earliness = 0
    #     sum_setup = 0
    #     for job in result:
    #         earliness = max(0, job.get('due_date') - job.get('end_date_production'))
    #         tardiness = max(0, job.get('end_date_production') - job.get('due_date'))
    #         setup = job.get('end_date_setup') - job.get('start_date')
    #         sum_tardiness += tardiness
    #         sum_earliness += earliness
    #         sum_setup += setup
    #     aof = weightings['earliness'] * sum_earliness + weightings['tardiness'] * sum_tardiness + weightings[
    #         'setup'] * sum_setup
    #     pareto.append({'idx': idx, 'aof': aof, 'earliness': sum_earliness, 'tardiness': sum_tardiness, 'setup': sum_setup})
    # pd.DataFrame.from_dict(pareto).to_csv('resources/pareto/ga_2.csv', index=False)


@timing
def run_ga_with_timing(ga, jobs, machines):
    last_best_individual = ga.population[0]
    epochs_since_last_change = 0
    epochs = 0
    while epochs < 1000 and epochs_since_last_change < 100:
        # print(epochs)
        ga.evolve_population(ga.num_parents, ga.selection_tournament_size, ga.num_cuts, ga.n_best, ga.replacement_tournament_size, ga.selection_operator, ga.crossover_operator, ga.mutation_operator, ga.replacement_operator)
        epochs += 1
        if last_best_individual == ga.population[0]:
            epochs_since_last_change += 1
        else:
            last_best_individual = ga.population[0]
            epochs_since_last_change = 0

    return ga_build_phenotype(ga.population[0], jobs, machines)


def ga_build_phenotype(chromosome: list, jobs: dict, machines: dict):
    from ga.machine import Job
    result = []
    for job_id in chromosome:
        job: Job = jobs.get(job_id)
        machine, completion_date = job.choose_production_slot(machines.values())
        machines.get(machine).schedule_job(job, completion_date)
        result.append({'job_id': job.job_id,
                       'machine_id': machine,
                       'material': job.material,
                       'due_date': job.due_date,
                       'end_date_setup': completion_date - job.valid_machines[machine],
                       'end_date_production': completion_date})
    setup_times = {}
    for machine in machines.values():
        setup_times.update(machine.calculate_setup_times())
        machine.schedule = []
    for res in result:
        setup = setup_times.get(res.get('job_id'))
        res['start_date'] = res.get('end_date_setup') - setup
    return result


def run_brd(setup_info: Dict[str, Dict], jobs_info: List[Dict], machines_info: List[Dict], prob: float,
            damping: float, is_tsp = False) -> Tuple[List[float], List[dict]]:
    from brd.game import SchedulingGame
    from brd.agent import JobAgent

    jobs = [JobAgent(job.get('name'),
                     job.get('material'),
                     job.get('due_date'),
                     job.get('valid_machines'))
            for job in jobs_info]

    machines = {m.get('name'): (m.get('init_material'),
                                m.get('init_date'))
                for m in machines_info}

    return run_brd_with_timing(jobs, setup_info, machines, prob, damping, is_tsp)


#@timing
def run_brd_with_timing(jobs, setup_info, machines, prob, damping, is_tsp):
    from brd.game import SchedulingGame, TourGame
    from brd.brd_algorithm import BestResponseDynamics

    jobs.sort(key=lambda job: (job.due_date, max(job.valid_machines.values())))

    if is_tsp:
        print('Running p-BRD on TSP problem with adjusted objective Function')
        game = TourGame(setup_matrix=setup_info, machines=machines, agents=jobs)
    else:
        game = SchedulingGame(setup_matrix=setup_info, machines=machines, agents=jobs)
    utilities, agents = BestResponseDynamics.run_kernel(game, prob=prob, damping_factor=damping)
    # utilities = None
    # agents = game.agents
    result = [{'job_id': job.name,
               'machine_id': job.machine,
               'material': job.material,
               'due_date': job.due_date,
               'start_date': job.setup_start,
               'end_date_setup': job.production_start,
               'end_date_production': job.production_end}
              for job in agents if job.name != 'INIT']
    return utilities, result

def run_oliver30(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size):
    from ga.genetic_algorithm import Oliver30GA

    cost_matrix = util.data_generation.get_oliver30_matrix()
    ga = Oliver30GA(genes=[i for i in range(30)], popsize=popsize, prob_x=prob_x, prob_mut=prob_mut,selection_operator=selection_operator, num_parents=num_parents, selection_tournament_size=selection_tournament_size, crossover_operator=crossover_operator, num_cuts=num_cuts, mutation_operator=mutation_operator, replacement_operator=replacement_operator, n_best=n_best, replacement_tournament_size=replacement_tournament_size, cost_matrix=cost_matrix)
    for _ in range(30000):
        ga.evolve_population(10)

    #print('Oliver30 GA fitnes: ',ga.fitness[0])
    #print('Oliver30 GA Population',ga.population[0])
    return ga.fitness[0], ga.population[0]


def main():
    setup, jobs, machines = util.data_generation.generate_TSP_benchmark()
    #setup, jobs, machines = util.data_generation.generate_eil51_TSP()

    # setup, jobs, machines = util.data_generation.generate_linear_benchmark()
    # setup, jobs, machines = util.data_generation.generate_minimal_benchmark()

    # setup, jobs, machines = util.data_generation.generate_realistic_data(num_materials=20,
    #                                                                      num_machines=10,
    #                                                                      num_jobs=50,
    #                                                                      urgency=0.2,  # less means more urgent
    #                                                                      spread=0.9)  # less means less spread


    #RUN realistic benchmarks
    #with open('resources/datasets/urgency.pkl', 'rb') as file:
    #   setup, jobs, machines = pickle.load(file)


    random.shuffle(jobs)

    # GA optimising for AOF
    ga_utilities, ga_result = run_ga(setup, jobs, machines, popsize=150, selection_tournament_size=3, num_parents=40, prob_x=0.9, prob_mut=0.35, replacement_tournament_size=5 )
    util.plotting.print_result('GA', ga_result, setup)
    util.plotting.plot_progress(ga_utilities, save_path='')
    util.plotting.plot_gantt(machines, ga_result, save_path='')

    # The GA for Oliver30, optimising for setuptimes only
    fitness, population = run_oliver30(popsize=30, prob_x=0.9, prob_mut=0.5, selection_operator="tournament", num_parents=5, selection_tournament_size=4, crossover_operator="singlepoint", num_cuts=-999, mutation_operator="swap", replacement_operator="tournament", n_best=-999, replacement_tournament_size="5")
    print(fitness)
    print(population)

    # BRD Algorithm, is_tsp controlls objective function
    brd_utilities, brd_result = run_brd(setup, jobs, machines, prob=0.28872271712504527, damping=0.542464731645394, is_tsp=False)  
    brd_result = sorted(brd_result, key=lambda k: k['end_date_production'])
    setups = util.plotting.print_result('p-BRD', brd_result, setup, is_tsp=True) # if is_tsp = True add distance from last to first to prints
    util.plotting.plot_progress(brd_utilities, save_path='')
    util.plotting.plot_gantt(machines, brd_result, save_path='')
    print(results)


if __name__ == '__main__':
    main()
