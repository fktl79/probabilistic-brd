from typing import List, Dict, Tuple

from auction.machine import Machine


class BidMessage:

    def __init__(self, job_id: str, machine_id: str, start_date: int, end_date_setup: int, end_date_production: int,
                 due_date: int, utility: float, material: str):
        self.job_id = job_id
        self.machine_id = machine_id
        self.start_date = start_date
        self.end_date_setup = end_date_setup
        self.end_date_production = end_date_production
        self.due_date = due_date
        self.utility = utility
        self.material = material


class Job:

    def __init__(self, job_id: str, due_date: int, material: str, valid_machines: Dict[str, int], setup: dict,
                 weightings: Dict[str, float]):
        self.job_id: str = job_id
        self.due_date: int = due_date
        self.material: str = material
        self.valid_machines: Dict[str, int] = valid_machines
        self.setup: Dict[str, int] = setup
        self.weightings: Dict[str, float] = weightings

    def get_bid_messages(self, free_machines: List[Machine], current_date: int) -> List[BidMessage]:
        """
        returns the BidMessages for the given machines
        """
        # determine if there is a machine where no setup is required
        machine_with_same_material = False
        for machine in free_machines:
            if machine.get_current_material() == self.material:
                machine_with_same_material = True
                break

        bid_messages = []
        for machine in free_machines:
            if machine.machine_id in self.valid_machines.keys():
                production_time = self.valid_machines[machine.machine_id]
                # calculate setup time
                setup_time, norm_setup_time = self._get_setup_time(machine.get_current_material())

                # calculate completion date
                start_date = machine.tmp_schedule if machine.tmp_schedule else len(machine.calendar)
                end_setup = machine.calculate_completion_date(current_date, setup_time)
                completion_date = end_setup + production_time

                # calculate earliness and tardiness
                earliness = max(0, self.due_date - completion_date)
                rel_earliness = earliness / production_time
                # if earliness > production_time:
                #     rel_earliness = 0
                # else:
                #     rel_earliness = 1 - earliness / production_time

                tardiness = max(0, completion_date - self.due_date)
                if tardiness == 0:
                    rel_tardiness = 0
                else:
                    rel_tardiness = 1 - tardiness / production_time
                #rel_tardiness = tardiness / production_time

                # eventually create BidMessage
                if self._is_bid_able(completion_date, setup_time, machine_with_same_material, production_time):
                    utility = self._calculate_utility(rel_earliness, rel_tardiness, norm_setup_time)
                    #if utility != 0:
                    bid_messages.append(BidMessage(self.job_id, machine.machine_id, start_date, end_setup,
                                                   completion_date, self.due_date, utility, self.material))

        return bid_messages

    def _get_setup_time(self, pre_material) -> Tuple[int, float]:
        """
        calculates the setup time for a given setup state of a machine
        """
        setup_time = self.setup.get(pre_material)
        max_setup = max(self.setup.values())
        norm_setup_time = setup_time / max_setup if max_setup != 0 else 1
        return setup_time, norm_setup_time

    def _is_bid_able(self, completion_date, setup_time, machine_with_same_material, production_time) -> int:
        """
        determines whether the job can bid under the current circumstances
        """
        if self.due_date - completion_date <= 0:
            return 1
        if self.due_date - self.weightings.get('can_bid_1') * (setup_time + production_time) <= completion_date:
            return 1
        if self.due_date - self.weightings.get('can_bid_2') * (setup_time + production_time) <= completion_date and machine_with_same_material:
            return 1
        return 0

    def _calculate_utility(self, rel_earliness: float, rel_tardiness: float,
                           norm_setup_time: float) -> float:
        """
        calculates the utility
        """
        lam_early = self.weightings['earliness']
        lam_tardy = self.weightings['tardiness']
        lam_setup = self.weightings['setup']

        if rel_tardiness > 0:
            lam_tardy = 1-lam_tardy

        return lam_early * rel_earliness + lam_tardy * rel_tardiness + lam_setup * norm_setup_time
