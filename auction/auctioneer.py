import pandas as pd
from typing import Dict, Union, List

from auction.machine import Machine
from auction.job import Job, BidMessage
from util.other import timing


class Auctioneer:

    def __init__(self, machines: Dict[str, Machine], jobs: Dict[str, Job]):
        self.current_date: int = 0
        self.machines: Dict[str, Machine] = machines
        self.jobs: Dict[str, Job] = jobs
        self.result: List[Dict[str, Union[str, int]]] = []

    #@timing
    def schedule_jobs(self) -> List[Dict[str, Union[str, int]]]:
        """
        runs the auction until all jobs are scheduled
        :return: the resulting machine assignment
        """
        # print('auction started')
        while self.jobs:
            winners = self._next_round()
            for job_id in winners:
                job = self.jobs.pop(job_id, None)
                #if job is not None:
                #    print('scheduled: {}'.format(job.job_id))
        # print('auction done.')
        return self.result

    def _next_round(self) -> List[str]:
        """
        initiates the next auction round
        :return: a list of job_ids that won the last auction
        """
        while True:
            free_machines = [machine for machine in self.machines.values() if machine.check_capacity(self.current_date)]
            if free_machines:
                winners = self._perform_auction(free_machines)
                if winners:
                    return winners
            self.current_date += 1

    def _perform_auction(self, free_machines: List[Machine]) -> List[str]:
        """
        manages the auction. Requests the bids and runs auction until final winners are determined
        :param free_machines: the machines that participate in the current auction
        :return: a list of job_ids that won the last auction (can be empty)
        """
        bid_messages: Dict[str, List[BidMessage]] = {}
        participants: List[Job] = []
        for job_id, job in self.jobs.items():
            bids = job.get_bid_messages(free_machines, self.current_date)
            if bids:
                participants.append(job)
                bid_messages[job_id] = bids

        bid_matrix = pd.DataFrame(index=[machine.machine_id for machine in free_machines], columns=bid_messages.keys())
        for job_id, bid_message_list in bid_messages.items():
            for bid_message in bid_message_list:
                bid_matrix.loc[bid_message.machine_id, job_id] = bid_message.utility

        # if no job wants to go on any of the free machines
        if bid_matrix.isnull().values.all():
            return []

        bid_matrix = bid_matrix.apply(pd.to_numeric)
        winners = self._find_tmp_winners(bid_matrix.copy(), {})

        while True:
            new_bids = self._notify_losers(winners, bid_messages, participants)
            if not new_bids:
                break
            for bid_message in new_bids:
                bid_matrix.loc[bid_message.machine_id,
                               bid_message.job_id] = bid_message.utility
                # bid_messages[bid_message.job_id].append(bid_message)
            next_winners = self._find_tmp_winners(bid_matrix.copy(), {})
            if winners == next_winners:
                break
            else:
                winners = next_winners

        self._notify_winners(winners, bid_messages)
        self._add_to_result(winners, bid_messages)
        return list(winners.values())

    def _find_tmp_winners(self, bid_matrix: pd.DataFrame, winners: Dict[str, str]) -> Dict[str, str]:
        """
        recursive algorithm that determines which job goes on which machine based on the jobs bids
        :param bid_matrix: the bid matrix
        :param winners: where to store the result
        :return: a dict in the form of {machine_id: job_id}
        """
        bid_matrix['winner'] = bid_matrix.idxmin(axis=1)
        v = bid_matrix['winner'].value_counts().reset_index().sort_values(['winner', 'index'], ascending=[False, True])
        if v.iloc[0]['winner'] > 1:
            first_index = v.iloc[0]['index']
            max_row = bid_matrix.loc[bid_matrix['winner'] == first_index, first_index].idxmin()
            winners[max_row] = first_index
            bid_matrix.drop([first_index, 'winner'], axis=1, inplace=True, errors='ignore')
            bid_matrix.drop(max_row, inplace=True)
            #bid_matrix.drop('winner', axis=1, inplace=True, errors='ignore')
            if bid_matrix.isnull().values.all() or bid_matrix.empty:
                return winners
            else:
                return self._find_tmp_winners(bid_matrix, winners)
        else:
            last_winners = bid_matrix['winner'].to_dict()
            return {**winners, **last_winners}  # merges two dicts

    def _notify_losers(self, tmp_winners: Dict[str, str], bid_messages: Dict[str, List[BidMessage]],
                       participants: List[Job]) -> List[BidMessage]:
        """
        requests new bids from losers based on who temporarily won the last auction.
        :param tmp_winners: the temporary winners of the last auction
        :param bid_messages: all initial bid messages
        :return: a list of new bid messages
        """
        # schedule machines with temporary winners
        machines = []
        for machine_id, job_id in tmp_winners.items():
            bid_message_list = bid_messages.get(job_id)
            if bid_message_list:
                for bid_message in bid_message_list:
                    if bid_message.machine_id == machine_id:
                        machine = self.machines.get(machine_id)
                        machine.schedule_temporary(bid_message.end_date_production, bid_message.material)
                        machines.append(machine)

        # request new bid messages from losers
        new_bid_messages = []
        for job in participants:
            if job.job_id not in tmp_winners.values():
                possible_machines = [self.machines[bid_message.machine_id] for bid_message in bid_messages[job.job_id]]
                bid_message_list = job.get_bid_messages(possible_machines, self.current_date)
                new_bid_messages.extend(bid_message_list)

        # remove temporary winners from machine schedule
        for machine in machines:
            machine.schedule_temporary(None, None)

        return new_bid_messages

    def _notify_winners(self, winners: Dict[str, str], bid_messages: Dict[str, List[BidMessage]]):
        """
        schedules the winning jobs on the machines
        :param winners: the winners of the auction in the form of {machine_id: job_id}
        :param bid_messages: all initial bid messages
        """
        for machine_id, job_id in winners.items():
            bid_message_list = bid_messages.get(job_id)
            if bid_message_list:
                for bid_message in bid_message_list:
                    if bid_message.machine_id == machine_id:
                        material = bid_message.material
                        date = bid_message.end_date_production
                        self.machines.get(machine_id).schedule_job(material, date)

    def _add_to_result(self, winners: Dict[str, str], bid_messages: Dict[str, List[BidMessage]]):
        """
        append winner information to result
        :param winners: the winners of the auction in the form of {machine_id: job_id}
        :param bid_messages: all initial bid messages
        """
        for machine_id, job_id in winners.items():
            bid_message_list = bid_messages.get(job_id)
            if bid_message_list:
                for bid_message in bid_message_list:
                    if bid_message.machine_id == machine_id:
                        job = self.jobs.get(job_id)
                        self.result.append({'job_id': job_id,
                                            'machine_id': machine_id,
                                            'material': job.material,
                                            'due_date': job.due_date,
                                            'start_date': bid_message.start_date,
                                            'end_date_setup': bid_message.end_date_setup,
                                            'end_date_production': bid_message.end_date_production})
