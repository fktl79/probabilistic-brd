from typing import List


class Machine:

    def __init__(self, machine_id: str, init_date: int, init_material: str):
        self.machine_id: str = machine_id
        self.calendar: List[str] = [init_material for _ in range(init_date)]
        self.tmp_schedule: int = None
        self.tmp_material: str = None

    def check_capacity(self, date: int) -> bool:
        return len(self.calendar) - 1 < date
        #return len(self.calendar) - 20 < date

    def get_current_material(self) -> str:
        if self.tmp_schedule:
            return self.tmp_material
        else:
            return self.calendar[-1]

    def calculate_completion_date(self, start_date: int, duration: int) -> int:
        last_scheduled_day = self.tmp_schedule if self.tmp_schedule else len(self.calendar)
        if start_date < last_scheduled_day:
            return last_scheduled_day + duration
        else:
            return start_date + duration

    def schedule_temporary(self, date: int, material: str):
        self.tmp_schedule = date
        self.tmp_material = material

    def schedule_job(self, material: str, until: int):
        for _ in range(until - len(self.calendar)):
            self.calendar.append(material)
