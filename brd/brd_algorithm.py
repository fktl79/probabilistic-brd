import functools
import random
from typing import List, Tuple
import copy
from .game import SchedulingGame
from .agent import JobAgent


class BestResponseDynamics:

    @staticmethod
    def run_kernel(game: SchedulingGame, prob=0, damping_factor=1, max_rounds=1000) -> Tuple[List[float], List[JobAgent]]:
        """
        executes the BRD algorithm and returns its progress
        :param game: the game
        :param prob: the probability that an agent swaps a position for the worse
        :param damping_factor: a factor that decreases (or increases) the probability at runtime
        :param max_rounds: the maximum number of iterations
        :return: the utility value for each iteration
        """
        game.utility = game.overall_utility()
        best_result: Tuple[float, List[JobAgent]] = (game.utility, copy.deepcopy(game.agents))
        utilities = []
        agent_improved = True
        i = 0
        while agent_improved and i < max_rounds:
            utilities.append(game.utility)
            agent_improved = functools.reduce(lambda x, y: x or y, map(lambda x: x.improve(game, prob), game.agents))
            prob *= damping_factor
            i += 1
            if game.utility < best_result[0]:
                best_result = (game.utility, copy.deepcopy(game.agents))
        return utilities, best_result[1]
