from __future__ import annotations
from typing import List, Dict, Tuple

from brd.agent import JobAgent


class SchedulingGame:

    def __init__(self, setup_matrix: Dict[str, Dict], machines: Dict[str, Tuple[str, int]], agents: List[JobAgent]):
        self.setup_matrix: Dict[str, Dict] = setup_matrix
        self.machines: Dict[str, Tuple[str, int]] = machines  # {name: (init_material, init_date)}
        self.utility = 0
        self.agents: List[JobAgent] = agents
        self.init_brd()

    def init_brd(self):
        # initialize the machines
        first_agents: List[JobAgent] = []
        for machine, (init_material, init_date) in self.machines.items():
            init_agent = JobAgent('INIT', init_material, init_date, {machine: init_date})
            init_agent.machine = machine
            init_agent.setup_start = 0
            init_agent.production_start = 0
            init_agent.production_end = init_date
            first_agents.append(init_agent)
        # find positions for agents
        for agent in self.agents:
            best_predecessor: Tuple[JobAgent, int] = (None, 0)  # its predecessor and its production end
            for a in first_agents:
                if a.machine in agent.valid_machines.keys():
                    pred, production_end = self.find_best_predecessor(a, agent)
                    if not best_predecessor[0]:
                        best_predecessor = (pred, production_end)
                    else:
                        # determine if pred is better than current best_predecessor
                        if abs(agent.due_date - production_end) < abs(agent.due_date - best_predecessor[1]):
                            best_predecessor = (pred, production_end)
            self.insert_after(best_predecessor[0], agent)
        self.agents.extend(first_agents)

    def find_best_predecessor(self, a: JobAgent, agent: JobAgent) -> Tuple[JobAgent, int]:
        # test if there is nothing scheduled on that machine
        if not a.successor:
            return a, self.find_agents_place_in_gap(a, agent)

        # first evaluate all gaps until the agents due date
        possible_predecessors: List[JobAgent] = []
        while a.production_end <= agent.due_date:
            if self.agent_fits_between(agent, pred=a, succ=a.successor):
                possible_predecessors.append(a)
            if a.successor:
                a = a.successor
            else:
                break
        # return best gap or find earliest gap after agent's due date
        best_predecessor = None
        if possible_predecessors:
            best_predecessor = possible_predecessors[-1]
        else:
            while not best_predecessor:
                if self.agent_fits_between(agent, pred=a, succ=a.successor):
                    best_predecessor = a
                else:
                    if a.successor:
                        a = a.successor
                    else:
                        # probably unnecessary
                        best_predecessor = a
        return best_predecessor, self.find_agents_place_in_gap(best_predecessor, agent)

    def agent_fits_between(self, agent: JobAgent, pred: JobAgent, succ: JobAgent):
        if not succ:
            return True
        gap_size = succ.production_start - pred.production_end
        needed_gap = self.get_setup_time(pred, agent) + agent.valid_machines[pred.machine] + self.get_setup_time(agent, succ)
        return gap_size > needed_gap

    def agents_can_swap(self, a1: JobAgent, a2: JobAgent) -> bool:
        if a1.successor == a2:
            pred = a1.predecessor
            succ = a2.successor
            if succ:
                gap = succ.production_start - pred.production_end
                needed_gap = self.get_setup_time(pred, a2) + a2.valid_machines[a1.machine] + \
                             self.get_setup_time(a2, a1) + a1.valid_machines[a2.machine] + \
                             self.get_setup_time(a1, succ)
                return gap > needed_gap
            else:
                return True
        elif a2.successor == a1:
            pred = a2.predecessor
            succ = a1.successor
            if succ:
                gap = succ.production_start - pred.production_end
                needed_gap = self.get_setup_time(pred, a1) + a1.valid_machines[a2.machine] + \
                             self.get_setup_time(a1, a2) + a2.valid_machines[a1.machine] + \
                             self.get_setup_time(a2, succ)
                return gap > needed_gap
            else:
                return True
        else:
            return self.agent_fits_between(a1, a2.predecessor, a2.successor) \
                   and self.agent_fits_between(a2, a1.predecessor, a1.successor)

    def get_setup_time(self, ag_from: JobAgent, ag_to: JobAgent) -> float:
        if ag_from and ag_to:
            return self.setup_matrix[ag_to.material][ag_from.material]
        else:
            return 0

    def find_agents_place_in_gap(self, pred: JobAgent, agent: JobAgent) -> int:
        prod_end = agent.due_date
        prod_time = agent.valid_machines[pred.machine]
        setup_pred_agent = self.get_setup_time(pred, agent)

        start = prod_end - prod_time - setup_pred_agent
        while start < pred.production_end:
            prod_end += 1
            start = prod_end - prod_time - setup_pred_agent

        if pred.successor:
            setup_agent_succ = self.get_setup_time(agent, pred.successor)
            end = prod_end + setup_agent_succ
            while end > pred.successor.production_start:
                prod_end -= 1
                end = prod_end + setup_agent_succ
        return prod_end

    def find_two_agents_place_in_gap(self, pred: JobAgent, a1: JobAgent, a2: JobAgent) -> int:
        prod_end = a2.due_date
        prod_time_a1 = a1.valid_machines[pred.machine]
        prod_time_a2 = a2.valid_machines[pred.machine]
        setup_pred_a1 = self.get_setup_time(pred, a1)
        setup_a1_a2 = self.get_setup_time(a1, a2)

        start = prod_end - prod_time_a2 - setup_a1_a2 - prod_time_a1 - setup_pred_a1
        while start < pred.production_end:
            prod_end += 1
            start = prod_end - prod_time_a2 - setup_a1_a2 - prod_time_a1 - setup_pred_a1

        if pred.successor:
            setup_a2_succ = self.get_setup_time(a2, pred.successor)
            end = prod_end + setup_a2_succ
            while end > pred.successor.production_start:
                prod_end -= 1
                end = prod_end + setup_a2_succ

        return prod_end

    def insert_after(self, pred: JobAgent, agent: JobAgent):
        agent.machine = pred.machine
        agent.predecessor = pred
        succ = pred.successor
        agent.successor = succ
        agent_end = self.find_agents_place_in_gap(pred, agent)
        agent.update_production_times(prod_end=agent_end)
        agent.update_setup_time(setup_time=self.get_setup_time(pred, agent))
        # this must happen after update_production_times
        pred.successor = agent
        if succ:
            succ.predecessor = agent
            succ.update_setup_time(self.get_setup_time(agent, succ))

    def swap_agents(self, a1: JobAgent, a2: JobAgent):
        if a1.successor == a2:
            self.swap_consecutive_agents(a1, a2)
        elif a2.successor == a1:
            self.swap_consecutive_agents(a2, a1)
        else:
            a1.machine, a2.machine = a2.machine, a1.machine
            pred1 = a1.predecessor
            pred2 = a2.predecessor
            self.cutout(a1)
            self.cutout(a2)
            # pred1 and pred2 will never be None because there are always InitAgents at beginning
            self.insert_after(pred1, a2)
            self.insert_after(pred2, a1)

    def swap_consecutive_agents(self, a1, a2):
        # first cut out both agents
        pred = a1.predecessor
        succ = a2.successor
        self.cutout(a1)
        self.cutout(a2)
        # then calculate end for both agents
        a1_end = self.find_two_agents_place_in_gap(pred, a2, a1)
        a1.update_production_times(a1_end)
        a1.update_setup_time(self.get_setup_time(a2, a1))
        a1.successor = succ
        a1.predecessor = pred
        pred.successor = a1
        if succ:
            succ.update_setup_time(self.get_setup_time(a1, succ))
            succ.predecessor = a1
        self.insert_after(pred, a2)

    def cutout(self, agent: JobAgent):
        pred = agent.predecessor
        succ = agent.successor
        if succ:
            pred.successor = succ
            succ.predecessor = pred
            succ.update_setup_time(self.get_setup_time(pred, succ))
        else:
            pred.successor = None
        agent.predecessor = None
        agent.successor = None

    def overall_utility(self, previous_utility=None):
        utility = 0
        for a in self.agents:
            if a.name != 'INIT':
                utility += a.calc_utility(self)
                # implemented for runtime optimisation
                if previous_utility:
                    if previous_utility < utility:
                        return utility

        return utility


class TourGame(SchedulingGame):
    def __init__(self, setup_matrix: Dict[str, Dict], machines: Dict[str, Tuple[str, int]], agents: List[JobAgent]):
        super().__init__(setup_matrix=setup_matrix, machines=machines, agents=agents)


    def overall_utility(self, previous_utility=None):
        #utility = super().overall_utility()
        utility = 0
        for a in self.agents:
            if a.name != 'INIT':
                utility += a.calc_utility_oliver30(self)
                # implemented for runtime optimisation
                if previous_utility:
                    if previous_utility < utility:
                        return utility

        # Add distance from last job to first job.
        sorted_agents = sorted(self.agents, key=lambda k: k.production_start)
        last = sorted_agents[-1].material
        first = sorted_agents[1].material # da es sich bei [0] um den init agent handelt
        utility += self.setup_matrix[last][first]
        return utility
