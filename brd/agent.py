from __future__ import annotations

import random

import brd.game
from typing import Dict


class JobAgent:

    def __init__(self, name: str, material: str, due_date: int, valid_machines: Dict[str, int]):
        self.name: str = name
        self.material: str = material
        self.due_date: int = due_date
        self.valid_machines: Dict[str, int] = valid_machines
        self.production_start: int = 0  # due_date - max()
        self.setup_start: int = self.production_start
        self.production_end: int = due_date
        self.machine: str = None
        self.predecessor: JobAgent = None
        self.successor: JobAgent = None

    def update_production_times(self, prod_end):
        self.production_end = prod_end
        self.production_start = prod_end - self.valid_machines[self.machine]

    def update_setup_time(self, setup_time):
        self.setup_start = self.production_start - setup_time

    def improve(self, game: brd.game.SchedulingGame, prob, coalition=None):
        # INIT Agents cannot improve
        if self.name == 'INIT':
            return False
        # find agent to change position with (or to schedule behind)
        for a in game.agents:
            if a != self and a.machine in self.valid_machines.keys():
                if a.name != 'INIT' and self.machine in a.valid_machines.keys():
                    # in this case a swap is worth considering
                    if game.agents_can_swap(self, a):
                        utility_before = game.utility
                        game.swap_agents(self, a)
                        utility_after = game.overall_utility(utility_before)
                        if utility_after < utility_before or random.random() < prob:
                            game.utility = utility_after
                            return True
                        else:
                            game.swap_agents(a, self)
                elif a.successor is None:
                    # in this case it is possible to move the agent behind a
                    utility_before = game.utility
                    old_pred = self.predecessor
                    game.cutout(self)
                    game.insert_after(a, self)
                    utility_after = game.overall_utility(utility_before)
                    if utility_after < utility_before:
                        game.utility = utility_after
                        return True
                    else:
                        game.cutout(self)
                        game.insert_after(old_pred, self)
        return False

    def calc_utility(self, game: brd.game.SchedulingGame):
        tardiness = max(0, self.production_end - self.due_date)
        earliness = max(0, self.due_date - self.production_end)
        setup_time = game.get_setup_time(self.predecessor, self)
        return tardiness + earliness + setup_time

    def calc_utility_oliver30(self, game: brd.game.SchedulingGame):
        setup_time = game.get_setup_time(self.predecessor, self)
        return setup_time
