import time
from abc import ABC, abstractmethod
from typing import List, Tuple, Dict
import random

from ga.machine import Machine
from ga.job import Job
# from util import timing
import logging
from multiprocessing import Pool
import os


class AbstractGA(ABC):

    def __init__(self, genes: List, popsize: int, prob_x: float, prob_mut: float, selection_operator: str, num_parents:int, selection_tournament_size: int, crossover_operator: str, num_cuts: int, mutation_operator: str, replacement_operator: str, n_best: int, replacement_tournament_size: int):
        self.population: List[List] = []
        self.popsize: int = popsize
        self.prob_x: float = prob_x
        self.prob_mut: float = prob_mut
        self._initialize_population(genes)
        #with Pool(os.cpu_count()-1) as pool:
        #    self.fitness: List = pool.map(self._calculate_fitness, self.population)
        self.fitness: List = [self._calculate_fitness(chromosome) for chromosome in self.population]
        # sort by fitness
        sorted_index = sorted(range(len(self.fitness)), key=self.fitness.__getitem__)  # ascending order
        self.fitness = [self.fitness[i] for i in sorted_index]
        self.population = [self.population[i] for i in sorted_index]

        #additional parameters
        self.selection_operator = selection_operator
        self.num_parents = num_parents
        self.selection_tournament_size = selection_tournament_size

        self.crossover_operator = crossover_operator
        self.num_cuts = num_cuts

        self.mutation_operator = mutation_operator

        self.replacement_operator = replacement_operator
        self.n_best = n_best
        self.replacement_tournament_size = replacement_tournament_size



    @abstractmethod
    def _initialize_population(self, genes: List):
        pass

    @abstractmethod
    def _calculate_fitness(self, chromosome: List) -> float:
        pass

    def _roulette_selection(self, num_parents: int):
        parents = []
        max = sum(self.fitness)
        while len(parents) != num_parents:
            pick = random.uniform(0, max)
            current = 0
            for i, chromosome in enumerate(self.population):
                current += self.fitness[i]
                if current > pick and chromosome not in parents:
                    parents.append(chromosome)
                    break
        return parents

    def _linear_rank_selection(self):
        pass

    def _tournament_selection(self, num_parents: int, tournament_size: int):
        parents = [] # array of parents

        while len(parents) != num_parents:
            # stores index of potential parents
            potential_parents = []

            for counter in range(tournament_size):  #pick index of k individuals at random
                random_individual_index = random.randint(0,len(self.population)-1)
                potential_parents.append(random_individual_index)

            #keep track of best individual and its fitness, initialise with first one in list
            best_individual_index = potential_parents[0]
            best_fitness = self.fitness[potential_parents[0]] #calculate fitness of index instead of individual. Use fitness list instead

            # find fittest individual in potential_parents list
            for pot_parent_index in potential_parents:
                if self.fitness[pot_parent_index] < best_fitness:  #todo: Eventuell kleiner fitness bei Minimierungsproblem. Checken!
                    best_fitness = self.fitness[pot_parent_index]
                    best_individual_index = pot_parent_index

            parents.append(self.population[best_individual_index]) # use self.population[best_individual instead]
        return parents

    def _single_point_crossover(self, parent1: List, parent2: List) -> Tuple[List, List]:
        # decide where to cut
        cut = random.randint(0, len(parent1))
        child1 = parent1[0:cut].copy()
        child2 = parent2[0:cut].copy()
        # fill tail
        for g in parent2[cut:]:
            if g not in child1:
                child1.append(g)
        for g in parent1[cut:]:
            if g not in child2:
                child2.append(g)
        # fill missing genes
        for g in parent2[0:cut]:
            if g not in child1:
                child1.append(g)
        for g in parent1[0:cut]:
            if g not in child2:
                child2.append(g)
        return child1, child2

    def _multi_point_crossover(self, parent1: List, parent2: List, num_cuts: int) -> Tuple[List, List]:
        #todo: muss sicherstellen, dass keine gene doppelt in chromosomen vorkommen! Hierzu check if it contains und dann fill missing genes wie in singlepoint crossover
        #determine cutpoints
        cuts = []
        for i in range(num_cuts):
             cut = random.randint(0, len(parent1))
             while cut in cuts:
                 cut = random.randint(0, len(parent1))
             cuts.append(cut)
        cuts.sort(reverse=False) # small values first
        cuts.append(-1) #add an extra value because we need one sublist more than cutpoints present in list

        #create sublists of genes
        child1_sublist = []
        child2_sublist = []
        for idx, cutpoint in enumerate(cuts):
            if cutpoint == -1:
                child1_sublist.append(parent1[cuts[idx-1]:len(parent1)])
                child2_sublist.append(parent2[cuts[idx-1]:len(parent2)])
            if idx == 0:
                child1_sublist.append(parent1[0:cutpoint])
                child2_sublist.append(parent2[0:cutpoint])
            else:
                child1_sublist.append(parent1[cuts[idx-1]:cutpoint])
                child2_sublist.append(parent2[cuts[idx-1]:cutpoint])

        # swap every second sublist (mod 2 != 0 check), create a single list of genes as child
        child1 = []
        child2 = []
        for position, subset in enumerate(child1_sublist):
            if position % 2 != 0: #swap if subsetnumber is odd
                child1.extend(child2_sublist[position])
                child2.extend(child1_sublist[position])
            else: # no swap
                child1.extend(child1_sublist[position])
                child2.extend(child2_sublist[position])
        return child1, child2

    def _uniform_crossover(self, parent1: List, parent2: List, prob: int) -> Tuple[List, List]:
        # does not preserve feasible solutions. Do not use it
        #initiate children
        child1 = parent1.copy()
        child2 = parent2.copy()
        # treat each gene independently, random choice which parent to inherit from
        for idx, gene_value in enumerate(parent1):
            if random.uniform(0,1)<prob: #swap the gene values with probability of prob
                temp_genevalue_storage = child1[idx]
                child1[idx] = child2[idx]
                child2[idx] = temp_genevalue_storage            # nicer alternative child1[idx], child2[idx] = child2[idx], child1[idx]
            else:   #no swapping
                pass
        return child1, child2 # treffe Entscheidung ebenfalls zwei children zurück zu geben. Man könnte auch nur 1 Kind generieren aus 2 Eltern.

    def _swap_mutate(self, child: List) -> List:
        first = random.randint(0, len(child)-1)
        second = random.randint(0, len(child)-1)
        while first == second:
            first = random.randint(0, len(child) - 1)
            second = random.randint(0, len(child) - 1)
        child[second], child[first] = child[first], child[second]
        return child

    def _random_mutate(self, child: List) -> List:
        current_pos = random.randint(0, len(child)-1)
        new_pos = random.randint(0, len(child)-1)
        while new_pos == current_pos:
            new_pos = random.randint(0, len(child))
        gene = child.pop(current_pos)
        child.insert(new_pos, gene)
        return child

    def _generational_replacement(self, children, n_best: int):
        # allow only children that are different from all existing chromosomes in the population
        children = [child for child in children if child not in self.population]
        children_fitness = [self._calculate_fitness(child) for child in children]

        # always copy best n member. Discard the rest of the former generation
        n_best_fitness = self.fitness[:n_best]
        n_best_individuals = self.population[:n_best]
        self.fitness = n_best_fitness
        self.population = n_best_individuals

        # add the generated children to the population
        # Important: len(Children) + n_best = pop_size ; Otherwise population will shrink over time
        self.fitness.extend(children_fitness)
        self.population.extend(children)

        sorted_index = sorted(range(len(self.fitness)), key=self.fitness.__getitem__)  # ascending order
        self.fitness = [self.fitness[i] for i in sorted_index]
        self.population = [self.population[i] for i in sorted_index]

        # other than that replace whole population
        pass #todo: improve: not enough children

    def _elitism_replacement(self, children):
        # allow only children that are different from all existing chromosomes in the population
        children = [child for child in children if child not in self.population]
        # remove by worst fitness
        #with Pool(os.cpu_count()-1) as pool:
        #    children_fitness = pool.map(self._calculate_fitness, children)
        children_fitness = [self._calculate_fitness(child) for child in children]
        self.population.extend(children)
        self.fitness.extend(children_fitness)
        sorted_index = sorted(range(len(self.fitness)), key=self.fitness.__getitem__)  # ascending order
        self.fitness = [self.fitness[i] for i in sorted_index]
        self.population = [self.population[i] for i in sorted_index]
        # cut = len(self.population)-self.popsize
        self.fitness = self.fitness[:self.popsize]
        self.population = self.population[:self.popsize]

    def _delete_n_last_replacement(self):
        pass

    def _delete_n_replacement(self):
        pass


    def _tournament_replacement(self, children: List, tournament_size: int): #todo
        # allow only children that are different from all existing chromosomes in the population
        children = [child for child in children if child not in self.population]
        children_fitness = [self._calculate_fitness(child) for child in children]

        for _ in children:
            #stores index of candidate individuals
            replacement_candidates = []
            # pick k individuals at random as canditates for replacement
            for counter in range(tournament_size):  #pick index of k individuals at random
                random_individual_index = random.randint(0,len(self.population)-1)
                replacement_candidates.append(random_individual_index)

            worst_individual_index = replacement_candidates[0] # index of worst individual
            worst_fitness = self.fitness[worst_individual_index] # fitnes of worst individual based on fitness list

            # find worst individual in this tournament
            for candidate in replacement_candidates:
                if self.fitness[candidate] > worst_fitness:
                    worst_fitness = self.fitness[candidate]
                    worst_individual_index = candidate
            #and remove entries in population and fitness lists
            self.population.pop(worst_individual_index)
            self.fitness.pop(worst_individual_index)

        self.population.extend(children)
        self.fitness.extend(children_fitness)
        # ascending order of population and fitness lists
        sorted_index = sorted(range(len(self.fitness)), key=self.fitness.__getitem__)  # ascending order
        self.fitness = [self.fitness[i] for i in sorted_index]
        self.population = [self.population[i] for i in sorted_index]


    def do_selection(self, operator_name: str, num_parents: int, tournament_size: int):
        parents = []
        if operator_name == 'roulette':
           parents = self._roulette_selection(num_parents)
        if operator_name == 'tournament':
           parents = self._tournament_selection(num_parents, tournament_size)
        return parents

    def do_crossover(self, operator_name: str, parent1: List, parent2: List, num_cuts=2):
        child1, child2 = [], []
        if operator_name == 'multipoint':
            child1, child2 = self._multi_point_crossover(parent1, parent2, num_cuts)
        if operator_name == 'singlepoint':
            child1, child2 = self._single_point_crossover(parent1, parent2)
        return child1, child2

    def do_mutate(self, operator_name: str, child: List ):
        final_child = []
        if operator_name == 'random':
            final_child = self._random_mutate(child)
        if operator_name == 'swap':
            final_child = self._swap_mutate(child)
        return final_child

    def do_replacement(self, operator_name: str, n_best: int, tournament_size: int, children: List):
        if operator_name == 'generational':
            self._generational_replacement(children=children, n_best=n_best)
        if operator_name == 'elitism':
            self._elitism_replacement(children=children)
        if operator_name == 'tournament':
            self._tournament_replacement(children=children, tournament_size=tournament_size)

    @abstractmethod
    def evolve_population(self, num_parents: int):
        pass


class Oliver30GA(AbstractGA):

    def __init__(self, genes: List, popsize: int, prob_x: float, prob_mut: float, cost_matrix: List[List], selection_operator: str, num_parents:int, selection_tournament_size: int, crossover_operator: str, num_cuts: int, mutation_operator: str, replacement_operator: str, n_best: int, replacement_tournament_size: int):
        self.cost_matrix = cost_matrix
        super().__init__(genes, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size)

    def _initialize_population(self, genes: List):
        for _ in range(self.popsize):
            chromosome = genes.copy()
            random.shuffle(chromosome)
            self.population.append(chromosome)

    def _calculate_fitness(self, chromosome: List) -> float:
        sum = 0
        for i in range(len(chromosome)):
            if i == 0:
                sum += self.cost_matrix[chromosome[len(chromosome) - 1]][chromosome[0]]
            else:
                first = chromosome[i - 1]
                second = chromosome[i]
                sum += self.cost_matrix[first][second]
        return sum

    def evolve_population(self,num_parents: int, selection_tournament_size=5, num_cuts=2, n_best=3, replacement_tournament_size=5, selection_operator = 'tournament', crossover_operator = 'singlepoint', mutation_operator = 'swap', replacement_operator = 'tournament'):
        #print('evolve population begin')
        # parent selection
        parents = self.do_selection(operator_name=selection_operator, num_parents=num_parents, tournament_size=selection_tournament_size)
        #print('selection done')
        # crossover
        children = []
        for i in range(0, len(parents) - 2, 2):
            if random.random() < self.prob_x:
                child1, child2 = self._single_point_crossover(parents[i], parents[i + 1])
                # mutation
                if random.random() < self.prob_mut:
                    child1 = self._swap_mutate(child1)
                if random.random() < self.prob_mut:
                    child2 = self._swap_mutate(child2)
                children.extend((child1, child2))
        # replacement
        #print('children produced')
        self.do_replacement(operator_name=replacement_operator, n_best=n_best, tournament_size=replacement_tournament_size, children=children)
        #print('replacements done')


class SchedulingGA(AbstractGA):

    def __init__(self, genes: List, popsize: int, prob_x: float, prob_mut: float, jobs: Dict[str, Job],
                 machines: Dict[str, Machine], selection_operator: str, num_parents:int, selection_tournament_size: int, crossover_operator: str, num_cuts: int, mutation_operator: str, replacement_operator: str, n_best: int, replacement_tournament_size: int):
        self.jobs: Dict[str, Job] = jobs
        self.machines: Dict[str, Machine] = machines
        self.utilities: List = []
        super().__init__(genes, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size)
        # print('init done')

    def _initialize_population(self, genes: List):
        for _ in range(self.popsize):
            chromosome = genes.copy()
            random.shuffle(chromosome)
            self.population.append(chromosome)

    def _calculate_fitness(self, chromosome: List) -> float:
        utility = 0
        for job_id in chromosome:
            job: Job = self.jobs.get(job_id)
            machine_id, slot = job.choose_production_slot(self.machines.values())
            utility += job.calc_tardiness(slot)
            utility += job.calc_earliness(slot)
            self.machines.get(machine_id).schedule_job(job, slot)

        for machine_id, machine in self.machines.items():
            setup_for_machine = machine.calculate_setup_times()
            for _, setup_time in setup_for_machine.items():
                utility += setup_time
            machine.schedule = []
        return utility

    def evolve_population(self, num_parents: int, selection_tournament_size=5, num_cuts=2, n_best=3, replacement_tournament_size=5, selection_operator = 'roulette', crossover_operator = 'singlepoint', mutation_operator = 'random', replacement_operator = 'elitism'): # this is the starting
        # parent selection
        parents = self.do_selection(operator_name=selection_operator, num_parents=num_parents, tournament_size=selection_tournament_size)
        # crossover
        children = []
        for i in range(0, len(parents) - 2, 2): #anzahl der eltern pro kind könnte man hier steuern indem man die  Zweien durch eine Variable ersetzt
            if random.random() < self.prob_x:
                child1, child2 = self.do_crossover(operator_name=crossover_operator, parent1=parents[i], parent2=parents[i + 1], num_cuts=num_cuts)
                # mutation
                if random.random() < self.prob_mut:
                    child1 = self.do_mutate(operator_name=mutation_operator, child=child1)
                if random.random() < self.prob_mut:
                    child2 = self.do_mutate(operator_name=mutation_operator, child=child2)
                children.extend((child1, child2))
        # replacement
        self.do_replacement(operator_name=replacement_operator, n_best=n_best, tournament_size=replacement_tournament_size, children=children,)
        self.utilities.append(self.fitness[0])
