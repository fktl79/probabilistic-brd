from __future__ import annotations
from typing import List, Tuple, Dict

import ga.machine
# from util import timing


class Job:

    def __init__(self, job_id: str, due_date: int, material: str, valid_machines: Dict[str, int]):
        self.job_id: str = job_id
        self.due_date: int = due_date
        self.material: str = material
        self.valid_machines: Dict[str, int] = valid_machines

    def choose_production_slot(self, machines: List[ga.machine.Machine]) -> Tuple[str, int]:
        best_slot = ()  # (Machine, completion_date)
        for machine in machines:
            if machine.machine_id in self.valid_machines.keys():
                slot = machine.find_slot_for_job(self)
                if not best_slot:
                    best_slot = slot
                else:
                    this_deviation = abs(self.due_date - slot[1])
                    best_deviation = abs(self.due_date - best_slot[1])
                    if this_deviation < best_deviation:
                        best_slot = slot
        return best_slot

    def calc_tardiness(self, completion_date: int) -> int:
        return max(0, completion_date - self.due_date)

    def calc_earliness(self, completion_date: int) -> int:
        return max(0, self.due_date - completion_date)
