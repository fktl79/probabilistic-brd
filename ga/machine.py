from __future__ import annotations
from typing import List, Dict, Tuple, Deque

from ga.job import Job
# from util import timing


class Machine:

    def __init__(self, machine_id: str, init_date: int, init_material: str, setup_matrix: Dict[str, Dict]):
        self.machine_id: str = machine_id
        self.init_date: int = init_date
        self.init_material: str = init_material
        self.setup_matrix: Dict[str, Dict[str, int]] = setup_matrix
        self.schedule: List[Tuple[Job, int]] = []

    def find_slot_for_job(self, job: Job) -> Tuple[str, int]:
        if not self.schedule:
            setup_time = self.setup_matrix[job.material][self.init_material]
            earliest_completion_date = self.init_date + setup_time + job.valid_machines[self.machine_id]
            if earliest_completion_date > job.due_date:
                completion_date = earliest_completion_date
            else:
                completion_date = job.due_date
        else:
            # evaluate scheduling before or after
            comp_before = self._evaluate_schedule_at_beginning(job)
            comp_after = self._evaluate_schedule_at_end(job)
            if comp_before == -1:
                completion_date = comp_after
            else:
                if abs(comp_after - job.due_date) < abs(comp_before - job.due_date):
                    completion_date = comp_after
                else:
                    completion_date = comp_before
        return self.machine_id, completion_date

    def _evaluate_schedule_at_beginning(self, job: Job):
        scheduled_job_info = self.schedule[0]
        scheduled_job = scheduled_job_info[0]
        gap_to_beginning = scheduled_job_info[1] - scheduled_job.valid_machines[self.machine_id] - self.init_date

        setup_time_before = self.setup_matrix[job.material][self.init_material]
        setup_time_after = self.setup_matrix[scheduled_job.material][job.material]
        if gap_to_beginning < setup_time_before + job.valid_machines[self.machine_id] + setup_time_after:
            return -1
        else:
            return scheduled_job_info[1] - scheduled_job.valid_machines[self.machine_id] - setup_time_after

    def _evaluate_schedule_at_end(self, job: Job):
        scheduled_job_info = self.schedule[-1]
        scheduled_job = scheduled_job_info[0]
        setup_time = self.setup_matrix[job.material][scheduled_job.material]
        return scheduled_job_info[1] + setup_time + job.valid_machines[self.machine_id]

    def schedule_job(self, job: Job, completion_date: int):
        if not self.schedule:
            self.schedule.append((job, completion_date))
        else:
            if completion_date < self.schedule[0][1]:
                self.schedule.insert(0, (job, completion_date))
            else:
                self.schedule.append((job, completion_date))

    def calculate_setup_times(self) -> Dict[str, int]:  # {job_id: setup_time}
        setup_times = {}
        predecessor = self.init_material
        for entry in self.schedule:
            job = entry[0]
            setup_times[job.job_id] = self.setup_matrix[job.material][predecessor]
            predecessor = job.material
        return setup_times
