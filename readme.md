# Algorithms for Parallel Machines Scheduling

This Code proposes three different algorithms to solve the parallel machines scheduling problem.
Those algorithms are run against different benchmarks (Oliver30, large, urgency, spread). The Oliver30 data set is generated based on a distance matrix that can be found in util/data_generation. The other data sets can be found in resources/datasets, they are generated using a more elaborate algorithm in order to imitatet realistic use cases. 

### Getting Started
1. clone repository
2. install dependencies (```pipenv install```)
3. run main.py

### Datasets

The datasets can be obtained in the corresponding directory above.
_urgency.pkl_
- num_materials=20,
- num_machines=10,
- num_jobs=50,
- urgency=0.01,
- spread=0.1

_spread.pkl_

- num_materials=20,
- num_machines=10,
- num_jobs=50,
- urgency=0.2,
- spread=0.9

     
_large.pkl_
- num_materials=30,
- num_machines=20,
- num_jobs=150,
- urgency=0.1,
- spread=0.5


_Oliver30_
- Based on the 2 Dimensional coordinates of 30 locations. 
- Euclidean Distance





#### Hyperparameter Tuning Results 

_GA_
| Parameter | Urgency| Spread | Large |
| ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ |
| popsize | 125 | 150 | 230 |
| selection_tm_size | 5 | 3 | 4 |
| num_parents | 40 | 40 | 56 |
| prob_x | 0.8 | 0.9 | 0.88 |
| prob_mutation | 0.35 | 0.35 | 0.31 |
| replace_tm_size | 7 | 5 | 30 |


_p-BRD_
| Parameter | Urgency| Spread | Large |
| ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ |
| probability | 0.8846 | 0.000124 | 0.8155 |
| damping_factor | 0.9884 | 0.8773 | 0.9896 |
