from typing import List, Dict
import pickle
from main import run_ga, run_oliver30
from multiprocessing import Process
from util.data_generation import generate_TSP_benchmark, get_oliver30_matrix


## This file creates the statistical distribution for the ga


num_repetitions = 750


######## Helper Functions #####################

def write_file(content: str, dataset: str):
    # write results in file
    with open(f'../experiments/GADistributionExperiment{dataset}.txt', 'a') as file:
        file.write(content+'\n')


def save_single_results(result: List[Dict], setup_matrix, run_index, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset): #todo: add all the different parameters of ga as function parameters
    sum_tardiness = 0
    sum_earliness = 0
    sum_setup = 0
    for job in result:
        earliness = max(0, job.get('due_date')-job.get('end_date_production'))
        tardiness = max(0, job.get('end_date_production')-job.get('due_date'))
        setup = job.get('end_date_setup')-job.get('start_date')
        sum_tardiness += tardiness
        sum_earliness += earliness
        sum_setup += setup

    aof = sum_earliness + sum_tardiness + sum_setup
    result_string = f'Index:{run_index}, Earliness:{sum_earliness}, Tardiness:{sum_tardiness}, Setup:{sum_setup}, AOF:{aof}'
    print(result_string)

    write_file(result_string, dataset)
    return sum_earliness, sum_tardiness, sum_setup, aof


###### Experiments ##################

def build_distribution_for_dataset(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size,dataset):
    run_experiment(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset=dataset)


def run_experiment(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset='oliver30'):
    # open fixed dataset.
    if dataset=="oliver30":
        setup_matrix, jobs, machines = generate_TSP_benchmark()
    else:
        with open(f'../resources/datasets/{dataset}.pkl', 'rb') as file:
           setup, jobs, machines = pickle.load(file)

    random.shuffle(jobs)

    # initiate arrays to compute mean and std of KPI's
    earliness = []
    tardiness = []
    setuptimes = []
    aofs = []

    #build savepath for figures
    save_figures_basefilename = f'pops{popsize}-probX{prob_x}-probMut{prob_mut}-selOP{selection_operator}-Parents{num_parents}-selTourSize{selection_tournament_size}-crossovOP{crossover_operator}-Cuts{num_cuts}-mutOP{mutation_operator}-repOP{replacement_operator}-nBest{n_best}-repTourSize{replacement_tournament_size}'
    save_figures_basefilename = save_figures_basefilename.replace('.', ',')

    # run same parameter combination X times
    for run_index in range(num_repetitions):
        print('run: ', run_index)
        # compute results
        if dataset == "oliver30":
            fitnes , population = run_oliver30(popsize=popsize, prob_x=prob_x, prob_mut=prob_mut,selection_operator=selection_operator, num_parents=num_parents, selection_tournament_size=selection_tournament_size, crossover_operator=crossover_operator, num_cuts=num_cuts, mutation_operator=mutation_operator, replacement_operator=replacement_operator, n_best=n_best, replacement_tournament_size=replacement_tournament_size)
            aofs.append(fitnes)
        else:
            ga_utilities, ga_result = run_ga(setup, jobs, machines, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size)
            #save results on disk
            sum_earliness, sum_tardiness, sum_setup, aof = save_single_results(ga_result, setup, run_index, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset) # ga results of each run with same parameter settings
            earliness.append(sum_earliness)
            tardiness.append(sum_tardiness)
            setuptimes.append(sum_setup)
            aofs.append(aof)
    if dataset == 'oliver30':
        with open('oliver30GA750Runs.data', 'wb') as filehandle:
            # store the data as binary data stream
            pickle.dump(aofs, filehandle)
        #util.plotting.print_result('GA', ga_result, setup)
        #util.plotting.plot_progress(ga_utilities, save_path=f'../experiments/figures/{save_figures_basefilename}ProgRun{run_index}data{dataset}.png') #
        #util.plotting.plot_gantt(machines, ga_result, save_path=f'../experiments/figures/{save_figures_basefilename}GanttRun{run_index}data{dataset}.png')
    return aofs



def build_ga_distributions():
    print("build ga distributions")
    #spread = Process(target=build_distribution_for_dataset, args=(150, 0.9, 0.35, "tournament", 40, 3, "singlepoint", -999, "swap", "tournament", -999 , 5, "spread",))
    #large = Process(target=build_distribution_for_dataset, args=(230, 0.881322, 0.317715, "tournament", 56, 4, "singlepoint", -999, "swap", "tournament",-999, 30, "large",))
    #urgency = Process(target=build_distribution_for_dataset, args=(125, 0.8, 0.35, "tournament", 40, 5, "singlepoint", -999, "swap", "tournament", -999, 7, "urgency",))
    oliver30 = Process(target=build_distribution_for_dataset, args=(270, 0.84555, 0.2163, "tournament", 4, 14, "singlepoint", -999, "swap", "tournament", -999, 22, "oliver30",))

    #spread.start()
    #large.start()
    #urgency.start()
    oliver30.start()

    #spread.join()
    #urgency.join()
    #large.join()
    oliver30.join()

def run_from_external():
    build_ga_distributions()

def load_oliver30_data():
    with open('oliver30GA750Runs.data', 'rb') as filehandle:
        # read the data as binary data stream
        list = pickle.load(filehandle)
    return list

if __name__ == '__main__':
    build_ga_distributions()
