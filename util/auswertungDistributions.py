import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
import statistics
import pickle

# This file analyses Distributions

plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42


def load_data(datasetname):
    index = []      #0
    earlines = []   #1
    tardines = []   #2
    setup = []      #3
    aofs = []       #4

    finaldata = {}
    finaldata['datasetname'] = datasetname.split('.')[0]

    with open(f'{datasetname}', 'r') as file:
        data = file.readlines()

        for line in data:
            splittedlist = line.split(', ')
            index.append(int(splittedlist[0].split(':')[1]))
            earlines.append(int(splittedlist[1].split(':')[1]))
            tardines.append(int(splittedlist[2].split(':')[1]))
            setup.append(int(splittedlist[3].split(':')[1]))
            aofs.append(int(splittedlist[4].strip().split(':')[1]))

    finaldata['earlines'] = earlines
    finaldata['tardines'] = tardines
    finaldata['setup'] = setup
    finaldata['aofs'] = aofs
    #print(finaldata, f'for {datasetname}')
    return finaldata


def compute_statistics_for_one_variable(datalist):
    mean = statistics.mean(datalist)
    std = statistics.stdev(datalist)
    return mean, std


# If the KS statistic is small or the p-value is high, then we cannot reject the hypothesis that the distributions of the two samples are the same.
    # H0: distributions of the two samples are the same
    # Halt: distributions of the two samples are not the same
# We can reject Nullhypothesis, if p value is smaller 0.05 or 0.10  --> conclusion distributions are different
def check_distribution_pairs(dist1, dist2):
    #teststat, p_value = stats.kstest(dist1, 'norm')
    teststat, p_value = stats.ks_2samp(dist1, dist2)
    return teststat, p_value


#dist1 is BRD, dist2 is GA
def kolmogorov_smirnov_one_sided(dist1, dist2):
    teststat, p_value = stats.ks_2samp(dist1, dist2, alternative='greater')
    return teststat, p_value


#Welsch T-test do not assume equal variances especially because of the spread dataset. Different variances.
def t_test(dist1, dist2):
    teststat, p_value = stats.ttest_ind(dist1, dist2, equal_var=False)
    return teststat, p_value

"""
Assumptions:
All the observations from both groups are independent of each other,
The responses are ordinal (i.e., one can at least say, of any two observations, which is the greater),
Under the null hypothesis H0, the distributions of both populations are equal.[3]
The alternative hypothesis H1 is that the distributions are not equal.

Under the general formulation, the test is only consistent when the following occurs under H1:
The probability of an observation from population X exceeding an observation from population Y is different (larger, or smaller) 
than the probability of an observation from Y exceeding an observation from X; i.e., P(X > Y) ≠ P(Y > X) or P(X > Y) + 0.5 · P(X = Y) ≠ 0.5.

Under more strict assumptions than the general formulation above, e.g., if the responses are assumed to be continuous and the alternative is restricted to a shift in location, i.e., F1(x) = F2(x + δ),
 we can interpret a significant Mann–Whitney U test as showing a difference in medians.
"""
def man_whitney_u_test(dist1, dist2):
    stat,p_value = stats.mannwhitneyu(dist1,dist2,alternative='two-sided')
    return stat, p_value


# plot histogram and a fitted kernel density estimate (KDE).
def plot_hist_single(dataset, filename, metric):
    #print(filename, "   " , metric)
    sns.set_style("ticks")
    if filename == 'p-BRDDistributionExperimentspread.txt':
        sns.set(color_codes=True)
        sns_distplot = sns.distplot(dataset, kde= False, hist_kws=dict(alpha=1))
        sns_distplot.get_figure().savefig(f"plots/{filename.split('.')[0]}-{metric}-Plot.eps", format='eps', dpi=600)

        sns.despine()
        plt.show()
        return
    else:
        sns.set(color_codes=True)
        sns_distplot = sns.distplot(dataset, hist_kws=dict(alpha=1))
        sns_distplot.get_figure().savefig(f"plots/{filename.split('.')[0]}-{metric}-Plot.eps", format='eps', dpi=600)

        sns.despine()
        plt.show()


# plot histogram and a fitted kernel density estimate (KDE). For GA and BRD on the same dataset in one plot (different colors) #Todo: pairwise plotting
# metric to set build filename
# filename one to find special case that destroys plot
def plot_hist_pairwise(dist1, dist2, filename1, metric):
    if filename1 == 'GADistributionExperimentspread.txt':
        sns.set(color_codes=True)
        sns.set_style("ticks")
        sns.despine()

        sns.distplot(dist1, label='GA' , kde=False, hist_kws=dict(alpha=1)) # label=f"GA-{filename1[24:].split('.')[0]}-{metric} "
        pairplot = sns.distplot(dist2, label ='p-BRD', kde=False, hist_kws=dict(alpha=1))  # label=f"p-BRD-{filename1[24:].split('.')[0]}-{metric}"

        sns.despine()
        plt.legend()
        pairplot.set(xlabel=metric.upper(), ylabel='Frequency', title=f'{metric.upper()} Distributions')
        pairplot.get_figure().savefig(f"plots/{filename1[24:].split('.')[0]}-{metric}-PairwisePlot.eps", format='eps', dpi=600)
        plt.show()
        return
    else:
        sns.set(color_codes=True)
        sns.set_style("ticks")
        sns.despine()

        sns.distplot(dist1, label=f"GA", hist_kws=dict(alpha=1))
        pairplot = sns.distplot(dist2, label=f"p-BRD", hist_kws=dict(alpha=1)) # -{filename1[24:].split('.')[0]}-{metric}

        sns.despine()
        plt.legend()
        pairplot.set(xlabel=metric.upper(), ylabel='Frequency', title=f'{metric.upper()} Distributions')
        pairplot.get_figure().savefig(f"plots/{filename1[24:].split('.')[0]}-{metric}-PairwisePlot.eps", format='eps', dpi=600)
        plt.show()


def normalize_list(raw):
    s = sum(raw)
    norm = [float(i)/s for i in raw]
    return norm


# By algorithm. Plot earliness for all three dataset (normalised)
def one_diagramm_every_dataset(dist1, dist2, dist3, filename1, metric, algorithm):

    dist1 = normalize_list(dist1)
    dist2 = normalize_list(dist2)
    dist3 = normalize_list(dist3)

    if filename1 == 'GADistributionExperimentspread.txt':
        sns.set(color_codes=True)

        sns.distplot(dist1, label=f"spread-{metric} ", kde=False , hist_kws=dict(alpha=1))
        sns.distplot(dist2,  label=f"urgency-{metric} ", kde=False, hist_kws=dict(alpha=1))
        complete_plot = sns.distplot(dist3, label=f"large-{metric}", kde=False, hist_kws=dict(alpha=1))

        sns.despine()
        plt.legend()

        complete_plot.get_figure().savefig(f"plots/{metric}--{algorithm}CompleteNormalisedPlot.eps", format='eps', dpi=600)
        plt.show()
        return
    else:
        sns.set(color_codes=True)

        sns.distplot(dist1, label=f"spread-{metric} ", hist_kws=dict(alpha=1))
        sns.distplot(dist2, label=f"urgency-{metric} ", hist_kws=dict(alpha=1))
        complete_plot = sns.distplot(dist3, label=f"large-{metric}", hist_kws=dict(alpha=1))

        sns.despine()
        plt.legend()

        complete_plot.get_figure().savefig(f"plots/{metric}--{algorithm}CompleteNormalisedPlot.eps", format='eps', dpi=600)
        plt.show()

def plot_triple_oliver30(ga, brd, brdSetup):
    #plot triple##
    sns.set(color_codes=True)
    #sns.set_style("white")
    sns.set_style("ticks")

    print(brdSetup)
    sns.distplot(ga,  label=f"GA", kde=False, hist_kws=dict(alpha=1))
    sns.distplot(brd,  label=f"p-BRD", kde=False, hist_kws=dict(alpha=1))
    tripleplot = sns.distplot(brdSetup,  label=f"BRD Setuptimes Only", kde=False, hist_kws=dict(alpha=1))

    sns.despine()
    plt.legend()
    tripleplot.set(xlabel='Route_Length', ylabel='Frequency', title='Oliver30 Route Length')
    tripleplot.get_figure().savefig(f"plots/Oliver30-TriplePlot.eps", format='eps', dpi=600)
    plt.show()

    ################

# additional: Oliver30 analysis
def oliver30_analysis(ga, brd, brdSetup):
    print('########## Analyse Oliver30 distributions #############')
    #Plot Pairise
    sns.set(color_codes=True)
    #sns.set_style("white")
    sns.set_style("ticks")

    sns.distplot(ga, label=f"GA", kde=False, hist_kws=dict(alpha=1))
    pairplot = sns.distplot(brd, label=f"p-BRD", kde=False, hist_kws=dict(alpha=1))

    sns.despine()
    plt.legend()
    pairplot.set(xlabel='Route Length', ylabel='Frequency', title='Oliver30 Route Length')
    pairplot.get_figure().savefig(f"plots/Oliver30-PairwisePlot.eps", format='eps', dpi=600)
    plt.show()
    #End plot pairwise

    plot_triple_oliver30(ga,brd,brdSetup)


    print('### Stat Tests ###')
    stat_aofs, p_aofs = check_distribution_pairs(ga, brd)
    print("Route_Length KS --> Stat: ", stat_aofs, "  P-Value ", p_aofs)

    stat_aofs, p_aofs = man_whitney_u_test(ga, brd)
    print("Route_Length MWU --> Stat: ", stat_aofs, "  P-Value ", p_aofs)

    print('### Statistics ###')
    print("Route_Length -> GA descriptive Statistics: ", compute_statistics_for_one_variable(ga))
    print("Route_Length -> p-BRD descriptive Statistics: ", compute_statistics_for_one_variable(brd))
    print("Route_Length -> p-BRD Setuptimes only descriptive Statistics: ", compute_statistics_for_one_variable(brdSetup))


# main method
def run_analysis(experiment_files_GA, experiment_files_BRD):
    # create filename array (whole)
    all_experiment_files = []
    all_experiment_files.extend(experiment_files_GA)
    all_experiment_files.extend(experiment_files_BRD)

    print("############### Plot Viz ###################")
    # plot visualizations of a single distribution
    for filename in all_experiment_files:
        parsed_file = load_data(filename)
        plot_hist_single(parsed_file['earlines'], filename, 'earliness')
        plot_hist_single(parsed_file['tardines'], filename, 'tardiness')
        plot_hist_single(parsed_file['setup'], filename, 'setup')
        plot_hist_single(parsed_file['aofs'], filename, 'aofs')

    print("##### Plot complete normalized plots ######")
    # plot pairwise figures as alternative visualisation
    one_diagramm_every_dataset(load_data(experiment_files_BRD[2])['earlines'], load_data(experiment_files_BRD[1])['earlines'], load_data(experiment_files_BRD[0])['earlines'], experiment_files_GA[2], 'Earliness', 'BRD')
    one_diagramm_every_dataset(load_data(experiment_files_BRD[2])['tardines'], load_data(experiment_files_BRD[1])['tardines'], load_data(experiment_files_BRD[0])['tardines'], experiment_files_GA[2], 'Tardiness', 'BRD')
    one_diagramm_every_dataset(load_data(experiment_files_BRD[2])['setup'], load_data(experiment_files_BRD[1])['setup'], load_data(experiment_files_BRD[0])['setup'], experiment_files_GA[2], 'Setup', 'BRD')
    one_diagramm_every_dataset(load_data(experiment_files_BRD[2])['aofs'], load_data(experiment_files_BRD[1])['aofs'], load_data(experiment_files_BRD[0])['aofs'], experiment_files_GA[2], 'AOF', 'BRD')

    one_diagramm_every_dataset(load_data(experiment_files_GA[2])['earlines'], load_data(experiment_files_GA[1])['earlines'], load_data(experiment_files_GA[0])['earlines'], experiment_files_GA[1], 'Earliness', 'GA')
    one_diagramm_every_dataset(load_data(experiment_files_GA[2])['tardines'], load_data(experiment_files_GA[1])['tardines'], load_data(experiment_files_GA[0])['tardines'], experiment_files_GA[1], 'Tardiness', 'GA')
    one_diagramm_every_dataset(load_data(experiment_files_GA[2])['setup'], load_data(experiment_files_GA[1])['setup'], load_data(experiment_files_GA[0])['setup'], experiment_files_GA[1], 'Setup', 'GA')
    one_diagramm_every_dataset(load_data(experiment_files_GA[2])['aofs'], load_data(experiment_files_GA[1])['aofs'], load_data(experiment_files_GA[0])['aofs'], experiment_files_GA[1], 'AOF', 'GA')

    print("############### Plot pairs and do tests ###################")
    # for each dataset (spread, large, urgency) compare distributions (created by different algorithms [BRD, GA]) for the different metrics
    for i in range(len(experiment_files_BRD)):
        # find comparable experiments
        parsed_file_ga = load_data(experiment_files_GA[i])
        parsed_file_brd = load_data(experiment_files_BRD[i])

        print("##### Plot pairs ######")
        # plot pairwise figures as alternative visualisation
        plot_hist_pairwise(parsed_file_ga['earlines'], parsed_file_brd['earlines'], experiment_files_GA[i], 'Earliness')
        plot_hist_pairwise(parsed_file_ga['tardines'], parsed_file_brd['tardines'], experiment_files_GA[i], 'Tardiness')
        plot_hist_pairwise(parsed_file_ga['setup'], parsed_file_brd['setup'], experiment_files_GA[i], 'Setup')
        plot_hist_pairwise(parsed_file_ga['aofs'], parsed_file_brd['aofs'], experiment_files_GA[i], 'AOF')

        print("##### do tests ######")
        print("KS Tests two-sided (equality)")
        # test distribution
        stat_earlines, p_earlines = check_distribution_pairs(parsed_file_ga['earlines'], parsed_file_brd['earlines'])
        stat_tardines, p_tardines = check_distribution_pairs(parsed_file_ga['tardines'], parsed_file_brd['tardines'])
        stat_setup, p_setup = check_distribution_pairs(parsed_file_ga['setup'], parsed_file_brd['setup'])
        stat_aofs, p_aofs = check_distribution_pairs(parsed_file_ga['aofs'], parsed_file_brd['aofs'])
        print("AOFs --> Stat: ", stat_aofs, "  P-Value ", p_aofs)
        print("setup --> Stat: ", stat_setup, "  P-Value ", p_setup)
        print("Tardiness --> Stat: ", stat_tardines, "  P-Value ", p_tardines)
        print("Earliness --> Stat: ", stat_earlines, "  P-Value ", p_earlines)

        print('### Mann-Whitney-U Tests')
        stat_earlines, p_earlines = man_whitney_u_test(parsed_file_ga['earlines'], parsed_file_brd['earlines'])
        stat_tardines, p_tardines = man_whitney_u_test(parsed_file_ga['tardines'], parsed_file_brd['tardines'])
        stat_setup, p_setup = man_whitney_u_test(parsed_file_ga['setup'], parsed_file_brd['setup'])
        stat_aofs, p_aofs = man_whitney_u_test(parsed_file_ga['aofs'], parsed_file_brd['aofs'])
        print("AOFs --> Stat: ", stat_aofs, "  P-Value ", p_aofs)
        print("setup --> Stat: ", stat_setup, "  P-Value ", p_setup)
        print("Tardiness --> Stat: ", stat_tardines, "  P-Value ", p_tardines)
        print("Earliness --> Stat: ", stat_earlines, "  P-Value ", p_earlines)

        print('### KS Test one-sided (greater), Alternativ Hypothesis: Metrics of BRD > Metrics of GA')
        stat_earlines, p_earlines = kolmogorov_smirnov_one_sided(parsed_file_brd['earlines'], parsed_file_ga['earlines'])
        stat_tardines, p_tardines = kolmogorov_smirnov_one_sided(parsed_file_brd['tardines'], parsed_file_ga['tardines'])
        stat_setup, p_setup = kolmogorov_smirnov_one_sided(parsed_file_brd['setup'], parsed_file_ga['setup'])
        stat_aofs, p_aofs = kolmogorov_smirnov_one_sided(parsed_file_brd['aofs'], parsed_file_ga['aofs'])
        print("If p-value <0.05 reject H0! That means evidence for BRD distribution better than GA")
        print("AOFs --> Stat: ", stat_aofs, "  P-Value ", p_aofs)
        print("setup --> Stat: ", stat_setup, "  P-Value ", p_setup)
        print("Tardiness --> Stat: ", stat_tardines, "  P-Value ", p_tardines)
        print("Earliness --> Stat: ", stat_earlines, "  P-Value ", p_earlines)

        print('### T-Test H0: Equal expected value, if p<0.05 reject H0')
        stat_earlines, p_earlines =t_test(parsed_file_brd['earlines'], parsed_file_ga['earlines'])
        stat_tardines, p_tardines = t_test(parsed_file_brd['tardines'], parsed_file_ga['tardines'])
        stat_setup, p_setup = t_test(parsed_file_brd['setup'], parsed_file_ga['setup'])
        stat_aofs, p_aofs = t_test(parsed_file_brd['aofs'], parsed_file_ga['aofs'])
        print("AOFs --> Stat: ", stat_aofs, "  P-Value ", p_aofs)
        print("setup --> Stat: ", stat_setup, "  P-Value ", p_setup)
        print("Tardiness --> Stat: ", stat_tardines, "  P-Value ", p_tardines)
        print("Earliness --> Stat: ", stat_earlines, "  P-Value ", p_earlines)

        print("AOFs -> GA descriptive Statistics: ", compute_statistics_for_one_variable(parsed_file_ga['aofs']))
        print("AOFs -> p-BRD descriptive Statistics: ", compute_statistics_for_one_variable(parsed_file_brd['aofs']))

def load_oliver30_data(algorithm):
    with open(f'oliver30{algorithm}750Runs.data', 'rb') as filehandle:
        # read the data as binary data stream
        list = pickle.load(filehandle)
    return list

def load_alternative_BRD(algorithm):
    with open(f'oliver30{algorithm}750RunsTSPfunctionNew.data', 'rb') as filehandle:
        # read the data as binary data stream
        list = pickle.load(filehandle)
    return list

if __name__ == "__main__":
    oliver30_analysis(load_oliver30_data('GA'),load_oliver30_data('BRD'),load_alternative_BRD('BRD'))
    #possible_datasets_GA = ['GADistributionExperimentlarge.txt', 'GADistributionExperimenturgency.txt', 'GADistributionExperimentspread.txt'] #'GADistributionExperimentspread.txt'
    #possible_datasets_BRD = ['p-BRDDistributionExperimentlarge.txt', 'p-BRDDistributionExperimenturgency.txt', 'p-BRDDistributionExperimentspread.txt'] #'p-BRDDistributionExperimentspread.txt'
    #run_analysis(possible_datasets_GA, possible_datasets_BRD)
