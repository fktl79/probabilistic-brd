from typing import List, Dict
import matplotlib.pyplot as plt

plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

def print_result(algorithm: str, result: List[Dict], setup_matrix, is_tsp=False):
    """
    Prints the KPIs of a given production plan to the console
    :param algorithm: the algorithm that was used
    :param result: the production plan
    :return: None
    """
    sum_tardiness = 0
    sum_earliness = 0
    sum_setup = 0
    for job in result:
        earliness = max(0, job.get('due_date')-job.get('end_date_production'))
        tardiness = max(0, job.get('end_date_production')-job.get('due_date'))
        setup = job.get('end_date_setup')-job.get('start_date')
        sum_tardiness += tardiness
        sum_earliness += earliness
        sum_setup += setup
    if is_tsp==True:
        # only for TSP benchmark
        sum_setup += setup_matrix[result[-1].get('material')][result[0].get('material')] # still neccessary for p-BRD solving TSP, because last to first not represented by an agent
        sum_setup = sum_setup / 1000  # Fixme


    aof = sum_earliness + sum_tardiness + sum_setup
    print('================')
    print('{} -> Earliness: {}, Tardiness: {}, Setup: {}, AOF: {}'.format(algorithm, sum_earliness, sum_tardiness,
                                                                          sum_setup, aof))
    return sum_setup


def plot_gantt(machines: List[Dict], schedule: List[Dict], title='', save_path=''):
    """
    Plots a GANTT chart representing the production plan resulting from the algorithms
    :param machines: the machines that got scheduled on
    :param schedule: the jobs that have been scheduled
    :param title: the title of the plot (optional)
    :param save_path: save plot to the given path (optional)
    :return: None
    """
    fig, ax = plt.subplots()
    y_labels = [m.get('name') for m in machines]
    ax.set_yticks(range(len(y_labels)))
    ax.set_yticklabels(y_labels)
    ax.set_xlabel('time')

    for machine in machines:
        ax.broken_barh([(0, machine.get('init_date'))],
                       (y_labels.index(machine.get('name')), 1),
                       facecolors=['green'])
        ax.annotate('INIT', (0 + 0.2, y_labels.index(machine.get('name')) + 0.5))

    for job in schedule:
        start_date = job.get('start_date')
        start_date_production = job.get('end_date_setup')
        setup_time = start_date_production - start_date
        production_time = job.get('end_date_production') - job.get('end_date_setup')
        ax.broken_barh([(start_date, setup_time), (start_date_production, production_time)],
                       (y_labels.index(job.get('machine_id')), 1),
                       facecolors=['orange', 'grey'], linestyle='-', edgecolor='black')
        ax.annotate('{}'.format(job.get('job_id')),
                    (start_date + 0.2, y_labels.index(job.get('machine_id')) + 0.5))

    ax.grid(True)
    if title:
        plt.title(title)
    if save_path:
        plt.savefig(save_path, dpi=600, format='eps')
    plt.show() #todo: use it to show plots again after each run of the algorithms
    plt.close('all')



def plot_progress(utilities: List[float], title='', save_path=''):
    """
    Plots the utilities that an algorithm achieved during runtime
    :param utilities: the utilities the algorithm reached
    :param title: the title of the plot (optional)
    :param save_path: save plot to the given path (optional)
    :return: None
    """
    fig, ax = plt.subplots()
    ax.plot(utilities)
    ax.set_xlabel('iteration')
    ax.set_ylabel('AOF')
    if title:
        plt.title(title)
    if save_path:
        plt.savefig(save_path, dpi=600, format='eps')
    plt.show() #todo: use it to show plots again
    plt.close('all')
