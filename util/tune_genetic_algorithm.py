from typing import List, Dict
import pickle
import util.plotting
import util.parameter_tuning
import util.data_generation
import statistics
from main import run_ga
from multiprocessing import Process
import numpy as np
import json


num_repetitions = 5 #number of genetic algorithm runs with same parameter settings. Mitigates stochastic effects

def write_file(content: str, dataset: str):
    # write results in file
    with open(f'../experiments/GAexperiment{dataset}.txt', 'a') as file:
        file.write(content+'\n')

#save best machine schedule to file (currently).
def write_best_result(best_aof,best_result, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset='spread' ):
    print('about to update best result')
    with open(f'bestresult{dataset}', 'w') as f:
        f.write(f'dataset{dataset}-aof{best_aof}-popsize{popsize}-probX{prob_x}-probMut{prob_mut}-selOP{selection_operator}-Parents{num_parents}-selTourSize{selection_tournament_size}-crossovOP{crossover_operator}-Cuts{num_cuts}-mutOP{mutation_operator}-repOP{replacement_operator}-nBest{n_best}-repTourSize{replacement_tournament_size}')
        f.write('/n')
        json.dump(best_result, f)
    return best_result

def compute_statistics(earliness, tardiness, setuptimes, aofs, dataset='spread'):
    # earlines statistics
    mean_earliness = statistics.mean(earliness)
    std_earliness = statistics.stdev(earliness)
    # tardiness statistics
    mean_tardiness = statistics.mean(tardiness)
    std_tardiness = statistics.stdev(tardiness)
    # setuptime statistics
    mean_setuptimes = statistics.mean(setuptimes)
    std_setuptimes = statistics.stdev(setuptimes)
    # aof statistics
    mean_aofs = statistics.mean(aofs)
    std_aofs = statistics.stdev(aofs)
    # file writting
    result_string = 'Statistics for dataset {}: meanEarliness:{}, stdEarliness:{}, meanTardiness:{}, stdTardiness:{}, meanSetup:{}, stdSetup:{}, meanAOF:{}, stdAOF:{}'.format(dataset, mean_earliness, std_earliness, mean_tardiness, std_tardiness, mean_setuptimes, std_setuptimes, mean_aofs, std_aofs)
    print(result_string)
    write_file(result_string, dataset=dataset)


def save_single_results(result: List[Dict], setup_matrix, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset): #todo: add all the different parameters of ga as function parameters
    sum_tardiness = 0
    sum_earliness = 0
    sum_setup = 0
    for job in result:
        earliness = max(0, job.get('due_date')-job.get('end_date_production'))
        tardiness = max(0, job.get('end_date_production')-job.get('due_date'))
        setup = job.get('end_date_setup')-job.get('start_date')
        sum_tardiness += tardiness
        sum_earliness += earliness
        sum_setup += setup
    # only for TSP benchmark
    # sum_setup += setup_matrix[result[-1].get('material')][result[0].get('material')]
    aof = sum_earliness + sum_tardiness + sum_setup
    print('================')
    result_string = f'Earliness:{sum_earliness}, Tardiness:{sum_tardiness}, Setup:{sum_setup}, AOF:{aof} --> popsize:{popsize}, probX:{prob_x}, probMut:{prob_mut}, selectionOperator:{selection_operator}, numParents:{num_parents}, selectionTourSize:{selection_tournament_size}, crossoverOperator:{crossover_operator}, Cuts:{num_cuts}, mutationOperator:{mutation_operator}, replaceOperator:{replacement_operator}, nBest:{n_best}, replaceTourSize:{replacement_tournament_size}'
    print(result_string)

    write_file(result_string, dataset)
    return sum_earliness, sum_tardiness, sum_setup, aof


def run_experiment(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset='spread'): #todo: add all the different parameters of ga as function parameters
    # open fixed dataset.
    with open(f'../resources/datasets/{dataset}.pkl', 'rb') as file:
       setup, jobs, machines = pickle.load(file)

    # initiate arrays to compute mean and std of KPI's
    earliness = []
    tardiness = []
    setuptimes = []
    aofs = []

    # save best result to file
    best_result = []
    best_aof = 99999999999

    #build savepath for figures
    save_figures_basefilename = f'pops{popsize}-probX{prob_x}-probMut{prob_mut}-selOP{selection_operator}-Parents{num_parents}-selTourSize{selection_tournament_size}-crossovOP{crossover_operator}-Cuts{num_cuts}-mutOP{mutation_operator}-repOP{replacement_operator}-nBest{n_best}-repTourSize{replacement_tournament_size}'
    save_figures_basefilename = save_figures_basefilename.replace('.', ',')

    # run same parameter combination X times
    for run_index in range(num_repetitions):
        # compute results
        ga_utilities, ga_result = run_ga(setup, jobs, machines, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size)
        #save results on disk
        sum_earliness, sum_tardiness, sum_setup, aof = save_single_results(ga_result, setup, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset) # ga results of each run with same parameter settings
        earliness.append(sum_earliness)
        tardiness.append(sum_tardiness)
        setuptimes.append(sum_setup)
        aofs.append(aof)

        #update currently best result and its corresponding aof
        if aof < best_aof:
            best_result = ga_result
            best_aof = aof

        #util.plotting.print_result('GA', ga_result, setup)
        util.plotting.plot_progress(ga_utilities, save_path=f'../experiments/figures/{save_figures_basefilename}ProgRun{run_index}data{dataset}.png') #
        util.plotting.plot_gantt(machines, ga_result, save_path=f'../experiments/figures/{save_figures_basefilename}GanttRun{run_index}data{dataset}.png')

    # write file to store best result and its aof
    write_best_result(best_aof, best_result,popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset='spread')

    # write computed statistics to file
    compute_statistics(earliness, tardiness, setuptimes, aofs, dataset=dataset)


def run_experiment_bayesian(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset='spread'): #todo: add all the different parameters of ga as function parameters
     # open fixed dataset.
    with open(f'../resources/datasets/{dataset}.pkl', 'rb') as file:
       setup, jobs, machines = pickle.load(file)

    # initiate arrays to compute mean and std of KPI's
    earliness = []
    tardiness = []
    setuptimes = []
    aofs = []

    # save best result to file
    #best_result = []
    #best_aof = 9999999999

    #build savepath for figures
    save_figures_basefilename = f'pops{popsize}-probX{prob_x}-probMut{prob_mut}-selOP{selection_operator}-Parents{num_parents}-selTourSize{selection_tournament_size}-crossovOP{crossover_operator}-Cuts{num_cuts}-mutOP{mutation_operator}-repOP{replacement_operator}-nBest{n_best}-repTourSize{replacement_tournament_size}'
    save_figures_basefilename = save_figures_basefilename.replace('.', ',')

    # run same parameter combination 5 times
    for run_index in range(num_repetitions):
        # compute results
        ga_utilities, ga_result = run_ga(setup, jobs, machines, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size)
        #save results on disk
        sum_earliness, sum_tardiness, sum_setup, aof = save_single_results(ga_result, setup, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset) # ga results of each run with same parameter settings
        earliness.append(sum_earliness)
        tardiness.append(sum_tardiness)
        setuptimes.append(sum_setup)
        aofs.append(aof)

        #update currently best result and its corresponding aof
        #if aof < best_aof:
        #    best_result = ga_result
        #    best_aof = aof

        #util.plotting.print_result('GA', ga_result, setup)
        #util.plotting.plot_progress(ga_utilities, save_path=f'../experiments/figures/{save_figures_basefilename}ProgRun{run_index}data{dataset}.png') #
        #util.plotting.plot_gantt(machines, ga_result, save_path=f'../experiments/figures/{save_figures_basefilename}GanttRun{run_index}data{dataset}.png')
    # write file to store best result and its aof
    #write_best_result(best_aof, best_result,popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset='spread')
    # write computed statistics to file
    compute_statistics(earliness, tardiness, setuptimes, aofs, dataset=(dataset+'Bayes'))
    return np.mean(aofs) #returns mean of number_of_repetions runs of the GA


def param_tuning(dataset):     # 'spread', 'large', 'urgency'
    #Suchraum
    popsizes = [90, 100, 125, 150, 200]
    prob_xs = [0.8, 0.9, 0.95]
    prob_muts = [0.05, 0.15, 0.2, 0.35]
    selection_operators = ['roulette', 'tournament']
    num_parents_candidates = [20, 30, 40]
    selection_tournament_sizes = [3, 5, 7, 12]
    crossover_operators = ['singlepoint'] #todo: multipointcrossover is not permutation safe yet
    num_cuts_candidates = [2, 3]  # multipointcrossover: number of cuttingpoints
    mutation_operators = ['random', 'swap']
    replacement_operators = ['elitism', 'tournament']  #generational is special case: need more individuals in next generation (num_parents + n_best = popsize)
    n_bests = [1, 3, 5, 8]
    replacement_tournament_sizes = [3, 5, 7, 12] # todo: eventuell nutze ich den gleichen Wert für selection und replacement

    popsizes = [125]
    prob_xs = [0.8]
    prob_muts = [0.35]
    selection_operators = ['tournament']
    num_parents_candidates = [40]
    selection_tournament_sizes = [5]
    crossover_operators = ['singlepoint'] #todo: multipointcrossover is not permutation safe yet
    num_cuts_candidates = [-1]  # multipointcrossover: number of cuttingpoints
    mutation_operators = ['swap']
    replacement_operators = ['tournament']  #generational is special case: need more individuals in next generation (num_parents + n_best = popsize)
    n_bests = [-1]
    replacement_tournament_sizes = [7] # todo: eventuell nutze ich den gleichen Wert für selection und replacement

    for popsize in popsizes:
        for prob_x in prob_xs:
            for prob_mut in prob_muts:
                for selection_operator in selection_operators:
                    for num_parents in num_parents_candidates:
                        for selection_tournament_size in selection_tournament_sizes: #special case do not need all the iterations of tournamentsize, if tournament is not the selected operator
                            for crossover_operator in crossover_operators:
                                for num_cuts in num_cuts_candidates: #special case. Besser einfach die special casses ganz nach innen packen und dort dann skippen
                                    for mutation_operator in mutation_operators:
                                        for replacement_operator in replacement_operators:
                                            for n_best in n_bests:
                                                for replacement_tournament_size in replacement_tournament_sizes: # todo: eventuell nutze ich den gleichen Wert für selection und replacement
                                                    run_experiment(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset=dataset)

#def param_tuning_bayesian():
#    aofs = []

#    for
#    mean_aof = max(aofs)

#    return mean_aof #mean aof of 5 runs

if __name__ == '__main__':
    #spread = Process(target=param_tuning, args=("spread",))
    #large = Process(target=param_tuning, args=("large",))
    urgency = Process(target=param_tuning, args=("urgency",))

    #spread.start()
    #large.start()
    urgency.start()

    #spread.join()
    urgency.join()
    #large.join()
    #param_tuning('large')
    #run_experiment(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size)
