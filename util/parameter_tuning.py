import pickle
from typing import List, Dict
from multiprocessing import Pool
import os
import logging

import numpy as np


##deprecated

def run_auction_in_parallel(filename: str, setup, jobs, machines):
    with Pool(os.cpu_count() - 1) as pool:
        result: List = pool.starmap(auction_param_tuning, [(x, setup, jobs, machines)
                                                           for x in np.arange(0.0, 1.1, 0.1)])

    print('all done.')
    with open('resources/param_tuning/{}.pkl'.format(filename), 'wb') as file:
        pickle.dump(result, file)


def auction_param_tuning(earliness_param, setup_matrix, jobs, machines):
    from main import run_auction

    result = {}
    for tardiness_param in np.arange(0.0, 1.1, 0.1):
        logging.warning('earliness: {}, tardiness: {}'.format(earliness_param, tardiness_param))
        for setup_param in np.arange(0.0, 1.1, 0.1):
            if earliness_param != 0 or tardiness_param != 0 or setup_param != 0:
                parameters = '{} - {} - {}'.format(earliness_param, tardiness_param, setup_param)
                # print(parameters)
                weightings = {'earliness': earliness_param,
                              'tardiness': tardiness_param,
                              'setup': setup_param,
                              'can_bid_1': 0.5,
                              'can_bid_2': 1.5}
                algo_result = run_auction(setup_matrix, jobs, machines, weightings)
                # result = write_result(algo_result, parameters, result)
                sum_tardiness = 0
                sum_earliness = 0
                sum_setup = 0
                for job in algo_result:
                    earliness = max(0, job['due_date'] - job['end_date_production'])
                    tardiness = max(0, job['end_date_production'] - job['due_date'])
                    setup = job['end_date_setup'] - job['start_date']
                    sum_tardiness += tardiness
                    sum_earliness += earliness
                    sum_setup += setup
                aof = sum_tardiness + sum_earliness + sum_setup
                result[parameters] = {'earliness': sum_earliness, 'tardiness': sum_tardiness,
                                      'setup': sum_setup, 'aof': aof}
    print('{} done'.format(earliness_param))
    return result


def run_brd_in_parallel(filename: str, setup, jobs, machines):
    with Pool(os.cpu_count() - 1) as pool:
        result: List = pool.starmap(brd_param_tuning, [(prob, setup, jobs, machines)
                                                       for prob in np.arange(0.0, 1.1, 0.1, 0.3, 0.5, 0.8)]) #0.3, 0.5, 0.8
    with open('resources/param_tuning/{}.pkl'.format(filename), 'wb') as file:
        pickle.dump(result, file)


def brd_param_tuning(prob, setup_matrix, jobs, machines):
    from main import run_brd
    result = {}
    for damping in np.arange(0.0, 1.1, 0.1, 0.3, 0.5, 0.8): # added 0.3, 0.5, 0.8
        parameters = '{} - {}'.format(prob, damping)
        utilities, algo_result = run_brd(setup_matrix, jobs, machines, prob, damping)
        sum_tardiness = 0
        sum_earliness = 0
        sum_setup = 0
        for job in algo_result:
            earliness = max(0, job['due_date'] - job['end_date_production'])
            tardiness = max(0, job['end_date_production'] - job['due_date'])
            setup = job['end_date_setup'] - job['start_date']
            sum_tardiness += tardiness
            sum_earliness += earliness
            sum_setup += setup
        aof = sum_tardiness + sum_earliness + sum_setup
        result[parameters] = {'earliness': sum_earliness, 'tardiness': sum_tardiness,
                              'setup': sum_setup, 'aof': aof, 'iterations': len(utilities)}
    return result


def write_result(result: List[Dict], parameters: str, collection: Dict) -> Dict:
    sum_tardiness = 0
    sum_earliness = 0
    sum_setup = 0
    for job in result:
        earliness = max(0, job.get('due_date') - job.get('end_date_production'))
        tardiness = max(0, job.get('end_date_production') - job.get('due_date'))
        setup = job.get('end_date_setup') - job.get('start_date')
        sum_tardiness += tardiness
        sum_earliness += earliness
        sum_setup += setup
    aof = sum_tardiness + sum_earliness + sum_setup
    collection[parameters] = {'earliness': sum_earliness, 'tardiness': sum_tardiness, 'setup': sum_setup, 'aof': aof}
    return collection
