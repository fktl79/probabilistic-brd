import csv
import itertools

filename = 'C:\\Users\\Blubs\\Desktop\\p-BRDexperimenturgency.txt'
#filename = 'C:\\Users\\Blubs\\Desktop\\GAexperimentspread.txt'


def read_complete_file(filename):
    with open(filename) as f:
        lines = f.readlines() #[line.rstrip() for line in f]
        lines = [line.rstrip() for line in lines]
    #print(lines[0:6])
    return lines

def get_every_nth_line(contentlist, n = 5):
    return contentlist[5::n]

def get_every_statistics_line(contentlist):
    statistics = []
    for line in contentlist:
        if 'Statistics' in line:
            statistics.append(line)
    return statistics

def get_every_standard_line(contentlist):
    lines = []
    for line in contentlist:
        if 'selectionOperator' in line:
            lines.append(line)
    return lines


def write_standard_csv(filename, contentlist, headerlist):
    with open(f'{filename}.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        headerlist.insert(0,'internalrun')
        headerlist.insert(0,'index')
        headerlist.insert(0,'5runsIndex')
        writer.writerow(headerlist)
        counter = 0
        for i, line in enumerate(contentlist):
            if ((i)%5) == 0:
                counter = counter + 1
            writer.writerow([counter,i+1,((i)%5), line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8],line[9], line[10], line[11], line[12], line[13], line[14], line[15]]) #i+1%5 wegen restklassen 0-4 zeigt an welcher run es war, i+1 ist die Zeile

def write_statistics_csv(filename, contentlist, headerlist):
    with open(f'{filename}.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        headerlist.insert(0,'index')
        writer.writerow([headerlist])
        for i, line in enumerate(contentlist):
            writer.writerow([i+1, line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7]]) #i+1%5 wegen restklassen 0-4 zeigt an welcher run es war, i+1 ist die Zeile

def unpack_statistics_line(line, dataset='urgency'):
    final_keys = []
    final_values = []

    temp = line.split(f'{dataset}:')
    temp = temp[1:][0]
    temp = temp.split(',')
    for entry in temp:
        temp_entry = entry.split(':')
        final_keys.append(temp_entry[0].strip())
        final_values.append(temp_entry[1])
    return final_keys,final_values


def unpack_standard_line(line):
    final_keys = []
    final_values = []
    temp = line.replace('-->', ',')
    temp = temp.split(',')

    for entry in temp:
        entry = entry.strip()
        temp_entry = entry.split(':')
        final_keys.append(temp_entry[0])
        final_values.append(temp_entry[1])
    return final_keys,final_values


### Skript

# get whole file content, print sample
content_list = read_complete_file(filename)

# get statistic lines, print sample
statistics = get_every_statistics_line(content_list)

# unpack statistics and reformat to csv. Save File
statistics_to_write = []
keys = []
for entry in statistics:
    #unpack
    key, values = unpack_statistics_line(entry,'urgency')
    #add to list of lists
    statistics_to_write.append(values)
    keys = key

# write statistics csv
write_statistics_csv(headerlist=keys, filename='AuswertungUrgencyStatisticsBRD', contentlist=statistics_to_write)


# write normal runs csv (1-5 mit selben Parametern)
standard = get_every_standard_line(content_list)

standard_to_write = []
keys_standard = []

for entry in standard:
    key, values = unpack_standard_line(entry)
    standard_to_write.append(values)
    keys_standard = key

write_standard_csv(headerlist=keys_standard, filename='AuswertungUrgencyStandardBrd', contentlist=standard_to_write)
