from typing import List, Dict
import util.plotting
import util.parameter_tuning
import util.data_generation
import statistics
from main import run_oliver30, run_ga, run_brd
from multiprocessing import Process
from hyperopt import hp, fmin, tpe, Trials, space_eval
import numpy as np
import json
import pickle


num_repetitions = 5 #number of genetic algorithm runs with same parameter settings. Mitigates stochastic effects

def write_file(content: str, dataset: str):
    # write results in file
    with open(f'../experiments/GAOliver30Experiment{dataset}.txt', 'a') as file:
        file.write(content+'\n')

#save best machine schedule to file (currently).
def write_best_result(best_aof,best_result, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset='spread' ):
    print('about to update best result')
    with open(f'bestresult{dataset}', 'w') as f:
        f.write(f'dataset{dataset}-aof{best_aof}-popsize{popsize}-probX{prob_x}-probMut{prob_mut}-selOP{selection_operator}-Parents{num_parents}-selTourSize{selection_tournament_size}-crossovOP{crossover_operator}-Cuts{num_cuts}-mutOP{mutation_operator}-repOP{replacement_operator}-nBest{n_best}-repTourSize{replacement_tournament_size}')
        f.write('/n')
        json.dump(best_result, f)
    return best_result

def compute_statistics(earliness, tardiness, setuptimes, aofs, dataset='spread'):
    # earlines statistics
    mean_earliness = statistics.mean(earliness)
    std_earliness = statistics.stdev(earliness)
    # tardiness statistics
    mean_tardiness = statistics.mean(tardiness)
    std_tardiness = statistics.stdev(tardiness)
    # setuptime statistics
    mean_setuptimes = statistics.mean(setuptimes)
    std_setuptimes = statistics.stdev(setuptimes)
    # aof statistics
    mean_aofs = statistics.mean(aofs)
    std_aofs = statistics.stdev(aofs)
    # file writting
    result_string = 'Statistics for dataset {}: meanEarliness:{}, stdEarliness:{}, meanTardiness:{}, stdTardiness:{}, meanSetup:{}, stdSetup:{}, meanAOF:{}, stdAOF:{}'.format(dataset, mean_earliness, std_earliness, mean_tardiness, std_tardiness, mean_setuptimes, std_setuptimes, mean_aofs, std_aofs)
    print(result_string)
    write_file(result_string, dataset=dataset)


def save_single_results(result: List[Dict], setup_matrix, popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset): #todo: add all the different parameters of ga as function parameters
    sum_tardiness = 0
    sum_earliness = 0
    sum_setup = 0
    for job in result:
        earliness = max(0, job.get('due_date')-job.get('end_date_production'))
        tardiness = max(0, job.get('end_date_production')-job.get('due_date'))
        setup = job.get('end_date_setup')-job.get('start_date')
        sum_tardiness += tardiness
        sum_earliness += earliness
        sum_setup += setup
    # only for TSP benchmark
    # sum_setup += setup_matrix[result[-1].get('material')][result[0].get('material')]
    aof = sum_earliness + sum_tardiness + sum_setup
    print('================')
    result_string = f'Earliness:{sum_earliness}, Tardiness:{sum_tardiness}, Setup:{sum_setup}, AOF:{aof} --> popsize:{popsize}, probX:{prob_x}, probMut:{prob_mut}, selectionOperator:{selection_operator}, numParents:{num_parents}, selectionTourSize:{selection_tournament_size}, crossoverOperator:{crossover_operator}, Cuts:{num_cuts}, mutationOperator:{mutation_operator}, replaceOperator:{replacement_operator}, nBest:{n_best}, replaceTourSize:{replacement_tournament_size}'
    print(result_string)

    write_file(result_string, dataset)
    return sum_earliness, sum_tardiness, sum_setup, aof


def run_experiment_bayesian_GA(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size, dataset='oliver30'):
    # initiate arrays to compute mean and std of KPI's
    tsp_results = []


    #build savepath for figures
    #save_figures_basefilename = f'pops{popsize}-probX{prob_x}-probMut{prob_mut}-selOP{selection_operator}-Parents{num_parents}-selTourSize{selection_tournament_size}-crossovOP{crossover_operator}-Cuts{num_cuts}-mutOP{mutation_operator}-repOP{replacement_operator}-nBest{n_best}-repTourSize{replacement_tournament_size}'
    #save_figures_basefilename = save_figures_basefilename.replace('.', ',')

    # run same parameter combination 5 times
    for run_index in range(num_repetitions):
        # compute results
        result_tsp, best_individual = run_oliver30(popsize=popsize, prob_x=prob_x, prob_mut=prob_mut, selection_operator=selection_operator, num_parents=num_parents, selection_tournament_size=selection_tournament_size, crossover_operator=crossover_operator, num_cuts=num_cuts, mutation_operator=mutation_operator, replacement_operator=replacement_operator, n_best=n_best, replacement_tournament_size=replacement_tournament_size)
        tsp_results.append(result_tsp)
    return np.mean(tsp_results) #returns mean of number_of_repetions runs of the oliver30 GA


def run_experiment_bayesian_BRD( prob=0.01, damping=0.9 ,dataset='oliver30'):
     # open fixed dataset.
    setup_matrix, jobs, machines = util.data_generation.generate_TSP_benchmark()

    # initiate arrays to compute mean and std of KPI's
    tsp_results = []

    # run same parameter combination 5 times
    for run_index in range(num_repetitions):
        # compute results
        brd_utilities, brd_result = run_brd(setup_matrix, jobs, machines, prob=prob, damping=damping, is_tsp=False)
        #util.plotting.print_result('p-BRD', brd_result, setup_matrix, is_tsp=True)
        sum_setup = 0
        for job in brd_result:
            setup = job.get('end_date_setup')-job.get('start_date')
            sum_setup += setup

        # only for TSP benchmark
        print('setup Before: ', sum_setup)
        sum_setup += setup_matrix[brd_result[-1].get('material')][brd_result[0].get('material')] # make it a circle by adding distance last to first scheduled job.
        sum_setup = sum_setup / 1000    # durch tausend teilen da setupmatrix für integer casting mit 1000 multipliziert wurde
        print('setup After: ', sum_setup)
        print(brd_result)
        tsp_results.append(sum_setup)
    return np.mean(tsp_results) #returns mean of number_of_repetions runs of the oliver30 BRD

###############################

#GA

###############################
'''
# relevant variables
random_state = 1
save_intervall = 25
repetitions = 800/save_intervall    # 750 controlls the number of target runs of the opt algorithm


def unpack(popsize, prob_x, prob_mut, num_parents_candidates, selection_tournament_size, replacement_tournament_size):
    return int(popsize), prob_x, prob_mut, int(num_parents_candidates), int(selection_tournament_size), int(replacement_tournament_size)


# define an objective function
def objective_function(args):
    popsize, prob_x, prob_mut, num_parents_candidates, selection_tournament_size, replacement_tournament_size = unpack(**args)
    # Annahmen: für operatoren aus vorherigen Tests (gegebenenfalls hinzufügen über hp.choice)
    return run_experiment_bayesian_GA(popsize=popsize, prob_x=prob_x, prob_mut=prob_mut, selection_operator='tournament', num_parents=num_parents_candidates, selection_tournament_size=selection_tournament_size,crossover_operator='singlepoint', num_cuts=-1, mutation_operator='swap', replacement_operator='tournament', n_best=-1, replacement_tournament_size=replacement_tournament_size, dataset='spread')


# define a search space
space = {
    'popsize': hp.quniform('popsize', 30, 300, 10),
    'prob_x': hp.uniform('prob_x', 0.75, 0.99),
    'prob_mut': hp.uniform('prob_mut', 0.01, 0.35),
    'num_parents_candidates': hp.quniform('num_parents_candidates', 5, 50, 4),
    'selection_tournament_size': hp.quniform('selection_tournament_size', 3, 30, 2),
    'replacement_tournament_size': hp.quniform('replacement_tournament_size', 3, 30, 2)
}


#run all iterations at once, without saving the states.
def run_bayesian_opt(evals=1500):
    #keep track of progress via trials object (list of dicts)
    trials = Trials()

    # minimize the objective over the space
    best = fmin(objective_function, space, algo=tpe.suggest, max_evals=evals, trials=trials, rstate=np.random.RandomState(random_state))

    # print best
    print('best: ',best)
    #hyperparams = space_eval(space, best)

    #print whole progress
    print(trials.trials)


    print('#####trials####')
    print(trials.best_trial)
    with open(f'BestTrialOliver30GA.txt', 'w') as f:
        f.write(str(trials.best_trial))


    pickle.dump(trials, open("bayesStateRunOnceGAOliver30.p", "wb"))
    trials = pickle.load(open("bayesStateRunOnceGAOliver30.p", "rb"))

# How to restart from existing
def restart_from_existing(save_after_x_runs, repetitions, start_from_iteration=0):
##### restart from pickled file

    # fill trials object with first entry
    trials = Trials()
    repetitionlist = list(range(repetitions))
    for index in repetitionlist[start_from_iteration:]:
        if index == 0:
            pass
        else:
            trials = pickle.load(open(f"bayesopt/bayesStateOliver30GA{index-1}.p", "rb"))
        # Perform an additional 100 evaluations
        # Note that max_evals is set to 200 because 100 entries already exist in the database
        best = fmin(objective_function,
            space=space,
            algo=tpe.suggest,
            trials=trials,
            max_evals=save_after_x_runs*(index+1),
            rstate=np.random.RandomState(random_state))

        # print interim results
        print(f'best of intervall {index}  : ', best)
        print(f'#####Best Trial of intervall {index}####')
        print(trials.best_trial)

        #save intervall
        pickle.dump(trials, open(f"bayesopt/bayesStateOliver30GA{index}.p", "wb"))

    print('#####Final Results######')
    trials = pickle.load(open(f"bayesopt/bayesStateOliver30GA{repetitions-1}.p", "rb")) # load last iteration
    print(trials.trials)
    print('###final best', trials.best_trial)
    with open(f'BestTrialSavePointsOliver30GA.txt', 'w') as f:
        f.write(str(trials.best_trial))
'''

###############################

#BRD

###############################

# relevant variables
random_state = 1
save_intervall = 25
repetitions = 800/save_intervall    # 750 controlls the number of target runs of the opt algorithm


def unpack(prob, damping):
    return prob, damping


# define an objective function
def objective_function(args):
    prob, damping = unpack(**args)
    # Annahmen: für operatoren aus vorherigen Tests (gegebenenfalls hinzufügen über hp.choice)
    return run_experiment_bayesian_BRD( prob=prob, damping=damping, dataset='oliver30')


# define a search space
space = {
    'prob': hp.uniform('prob', 0.0, 0.99),
    'damping': hp.uniform('damping', 0.0, 0.99),
}


#run all iterations at once, without saving the states.
def run_bayesian_opt(evals=850):
    #keep track of progress via trials object (list of dicts)
    trials = Trials()

    # minimize the objective over the space
    best = fmin(objective_function, space, algo=tpe.suggest, max_evals=evals, trials=trials, rstate=np.random.RandomState(random_state))

    # print best
    print('best: ',best)
    #hyperparams = space_eval(space, best)

    #print whole progress
    print(trials.trials)


    print('#####trials####')
    print(trials.best_trial)
    with open(f'BestTrialOliver30BRD.txt', 'w') as f:
        f.write(str(trials.best_trial))


    pickle.dump(trials, open("bayesStateRunOnceBRDOliver30.p", "wb"))
    trials = pickle.load(open("bayesStateRunOnceBRDOliver30.p", "rb"))

# How to restart from existing
def restart_from_existing(save_after_x_runs, repetitions, start_from_iteration=0):
##### restart from pickled file

    # fill trials object with first entry
    trials = Trials()
    repetitionlist = list(range(repetitions))
    for index in repetitionlist[start_from_iteration:]:
        if index == 0:
            pass
        else:
            trials = pickle.load(open(f"bayesopt/bayesStateOliver30BRD{index-1}.p", "rb"))
        # Perform an additional 100 evaluations
        # Note that max_evals is set to 200 because 100 entries already exist in the database
        best = fmin(objective_function,
            space=space,
            algo=tpe.suggest,
            trials=trials,
            max_evals=save_after_x_runs*(index+1),
            rstate=np.random.RandomState(random_state))

        # print interim results
        print(f'best of intervall {index}  : ', best)
        print(f'#####Best Trial of intervall {index}####')
        print(trials.best_trial)

        #save intervall
        pickle.dump(trials, open(f"bayesopt/bayesStateOliver30BRD{index}.p", "wb"))

    print('#####Final Results######')
    trials = pickle.load(open(f"bayesopt/bayesStateOliver30BRD{repetitions-1}.p", "rb")) # load last iteration
    print(trials.trials)
    print('###final best', trials.best_trial)
    with open(f'BestTrialSavePointsOliver30BRD.txt', 'w') as f:
        f.write(str(trials.best_trial))



#################################

#################################


if __name__ == '__main__':
    print("Oliver30 Experiments")
    #oliver30_GA = Process(target=param_tuning_bayesian_GA, args=("urgency",))
    #oliver30_BRD = Process(target=run_experiment_bayesian_BRD, args=("urgency",))


    #oliver30_GA.start()
    #oliver30_BRD.start()

    #oliver30_GA.join()
    #oliver30_BRD.join()

    #run_experiment_bayesian_GA(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size)
    #run_experiment_bayesian_BRD(popsize, prob_x, prob_mut, selection_operator, num_parents, selection_tournament_size, crossover_operator, num_cuts, mutation_operator, replacement_operator, n_best, replacement_tournament_size)

    run_bayesian_opt(500)
    #restart_from_existing(save_intervall, int(repetitions), start_from_iteration=0) #start from iteration lastfileindex+1, starts from savepoint
