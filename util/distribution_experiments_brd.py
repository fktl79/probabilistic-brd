import pickle
from main import run_brd
from multiprocessing import Process
num_repetitions = 750
from util.data_generation import generate_TSP_benchmark
import random

## This file creates the statistical distribution for the p-BRD ###

def write_file(content: str, dataset: str):
    # write results in file
    with open(f'../experiments/p-BRDDistributionExperiment{dataset}.txt', 'a') as file:
        file.write(content+'\n')


def run_experiment(prob, damping, init_state, dataset='oliver30'):
    if dataset=="oliver30":
        setup_matrix, jobs, machines = generate_TSP_benchmark()
        is_tsp = True
    else:
        is_tsp = False
        with open(f'../resources/datasets/{dataset}.pkl', 'rb') as file:
            setup_matrix, jobs, machines = pickle.load(file)
    
    random.shuffle(jobs)

    # initiate arrays to compute mean and std of KPI's
    earliness_list = []
    tardiness_list = []
    setuptimes = []
    aofs = []

    save_figures_basefilename = f'prob{prob}-damping{damping}-dataset{dataset}'
    save_figures_basefilename = save_figures_basefilename.replace('.', ',')

    for run_index in range(num_repetitions):
        parameters = '{} - {}'.format(prob, damping)
        utilities, algo_result = run_brd(setup_matrix, jobs, machines, prob, damping, is_tsp=is_tsp) # Fixme: false is standard!
        if dataset=="oliver30":
            algo_result = sorted(algo_result, key=lambda k: k['end_date_production'])  # addition for tsp

        sum_tardiness = 0
        sum_earliness = 0
        sum_setup = 0
        for job in algo_result:
            earliness = max(0, job['due_date'] - job['end_date_production'])
            tardiness = max(0, job['end_date_production'] - job['due_date'])
            setup = job['end_date_setup'] - job['start_date']
            sum_tardiness += tardiness
            sum_earliness += earliness
            sum_setup += setup
        if dataset == "oliver30":
            print('Before: ',sum_setup)
            sum_setup += setup_matrix[algo_result[-1].get('material')][algo_result[0].get('material')]
            sum_setup = sum_setup / 1000  # Fixme
            print('After: ',sum_setup)
        aof = sum_tardiness + sum_earliness + sum_setup

        earliness_list.append(sum_earliness)
        tardiness_list.append(sum_tardiness)
        setuptimes.append(sum_setup)
        aofs.append(aof)

        #save single result to file. Compute Distribution later
        result_string = f'Index:{run_index}, Earliness:{sum_earliness}, Tardiness:{sum_tardiness}, Setup:{sum_setup}, AOF:{aof}'
        print(result_string)
        write_file(result_string, dataset)

    if dataset == 'oliver30':
        with open('oliver30BRD750RunsTSPfunctionNew.data', 'wb') as filehandle:
            # store the data as binary data stream
            pickle.dump(setuptimes, filehandle)
    return aofs


def build_distribution_for_dataset(prob, damping, dataset):
    run_experiment(prob, damping, None, dataset=dataset)


def build_brd_distributions():
    print("build brd distributions")
    #spread = Process(target=build_distribution_for_dataset, args=(0.0012414693228188667, 0.8773616776537311, "spread",))
    #large = Process(target=build_distribution_for_dataset, args=( 0.8155382003421695 ,0.989651502391356, "large",))
    #urgency = Process(target=build_distribution_for_dataset, args=(0.8846322131215641, 0.9884501078046565, "urgency",))
    oliver30 = Process(target=build_distribution_for_dataset, args=(0.28872271712504527, 0.542464731645394, "oliver30",))

    #spread.start()
    #large.start()
    #urgency.start()
    oliver30.start()

    #spread.join()
    #urgency.join()
    #large.join()
    oliver30.join()


def run_from_external():
    build_brd_distributions()
    #run_experiment(0.8155382003421695, 0.989651502391356, "large")

def load_oliver30_data():
    with open('oliver30BRD750RunsTSPfunctionNew.data', 'rb') as filehandle:
        # read the data as binary data stream
        list = pickle.load(filehandle)
    return list

if __name__ == '__main__':
    #build_brd_distributions()
    build_distribution_for_dataset(prob=0.28872271712504527, damping=0.542464731645394, dataset="oliver30")
    #run_experiment(0.8155382003421695, 0.989651502391356, "large")
    print(load_oliver30_data())
    print([x for x in load_oliver30_data() if x < 419])
