from util.tune_genetic_algorithm import run_experiment_bayesian
from hyperopt import hp, fmin, tpe, Trials, space_eval
import numpy as np
import json
import pickle

#searchspace
#popsizes = [90, 100, 125, 150, 200]
#prob_xs = [0.8, 0.9, 0.95]
#prob_muts = [0.05, 0.15, 0.2, 0.35]
#selection_operators = ['roulette', 'tournament']
#num_parents_candidates = [20, 30, 40]
#selection_tournament_sizes = [3, 5, 7, 12]
#crossover_operators = ['singlepoint'] #todo: multipointcrossover is not permutation safe yet
#num_cuts_candidates = [2, 3]  # multipointcrossover: number of cuttingpoints
#mutation_operators = ['random', 'swap']
#replacement_operators = ['elitism', 'tournament']  #generational is special case: need more individuals in next generation (num_parents + n_best = popsize)
#n_bests = [1, 3, 5, 8]
#replacement_tournament_sizes = [3, 5, 7, 12] # todo: eventuell nutze ich den gleichen Wert für selection und replacement

# relevant variables
random_state = 1
save_intervall = 25
repetitions = 750/save_intervall    # 750 controlls the number of target runs of the opt algorithm


def unpack(popsize, prob_x, prob_mut, num_parents_candidates, selection_tournament_size, replacement_tournament_size):
    return int(popsize), prob_x, prob_mut, int(num_parents_candidates), int(selection_tournament_size), int(replacement_tournament_size)


# define an objective function
def objective_function(args):
    popsize, prob_x, prob_mut, num_parents_candidates, selection_tournament_size, replacement_tournament_size = unpack(**args)
    # Annahmen: für operatoren aus vorherigen Tests (gegebenenfalls hinzufügen über hp.choice)
    return run_experiment_bayesian(popsize, prob_x, prob_mut, 'tournament', num_parents_candidates, selection_tournament_size,'singlepoint', -1, 'swap', 'tournament', -1, replacement_tournament_size, dataset='spread')


# define a search space
space = {
    'popsize': hp.quniform('popsize', 120, 300, 10),
    'prob_x': hp.uniform('prob_x', 0.75, 0.95),
    'prob_mut': hp.uniform('prob_mut', 0.05, 0.35),
    'num_parents_candidates': hp.quniform('num_parents_candidates', 20, 55, 4),
    'selection_tournament_size': hp.quniform('selection_tournament_size', 4, 40, 2),
    'replacement_tournament_size': hp.quniform('replacement_tournament_size', 4, 40, 2)
}


#run all iterations at once, without saving the states.
def run_bayesian_opt(evals):
    #keep track of progress via trials object (list of dicts)
    trials = Trials()

    # minimize the objective over the space
    best = fmin(objective_function, space, algo=tpe.suggest, max_evals=evals, trials=trials, rstate=np.random.RandomState(random_state))

    # print best
    print('best: ',best)
    #hyperparams = space_eval(space, best)

    #print whole progress
    print(trials.trials)


    print('#####trials####')
    print(trials.best_trial)
    with open(f'BestTrial.txt', 'w') as f:
        f.write(str(trials.best_trial))


    pickle.dump(trials, open("bayesStateRunOnceGA.p", "wb"))
    trials = pickle.load(open("bayesStateRunOnceGA.p", "rb"))


# How to restart from existing
def restart_from_existing(save_after_x_runs, repetitions, start_from_iteration=0):
##### restart from pickled file

    # fill trials object with first entry
    trials = Trials()
    repetitionlist = list(range(repetitions))
    for index in repetitionlist[start_from_iteration:]:
        if index == 0:
            pass
        else:
            trials = pickle.load(open(f"bayesopt/bayesState{index-1}.p", "rb"))
        # Perform an additional 100 evaluations
        # Note that max_evals is set to 200 because 100 entries already exist in the database
        best = fmin(objective_function,
            space=space,
            algo=tpe.suggest,
            trials=trials,
            max_evals=save_after_x_runs*(index+1),
            rstate=np.random.RandomState(random_state))

        # print interim results
        print(f'best of intervall {index}  : ', best)
        print(f'#####Best Trial of intervall {index}####')
        print(trials.best_trial)

        #save intervall
        pickle.dump(trials, open(f"bayesopt/bayesState{index}.p", "wb"))

    print('#####Final Results######')
    trials = pickle.load(open(f"bayesopt/bayesState{repetitions-1}.p", "rb")) # load last iteration
    print(trials.trials)
    print('###final best', trials.best_trial)
    with open(f'BestTrialSavePoints.txt', 'w') as f:
        f.write(str(trials.best_trial))


if __name__ == '__main__':
    restart_from_existing(save_intervall, int(repetitions), start_from_iteration=0) #start from iteration lastfileindex+1, starts from savepoint
