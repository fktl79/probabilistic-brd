import statistics
from main import run_brd
import numpy as np
from util.tune_genetic_algorithm import run_experiment_bayesian
from hyperopt import hp, fmin, tpe, Trials, space_eval
import pickle
from multiprocessing import Process



#result = {}
num_repetitions = 5

def write_file(content: str, dataset: str):
    # write results in file
    with open(f'../experiments/p-BRDexperiment{dataset}.txt', 'a') as file:
        file.write(content+'\n')


def compute_statistics(earliness, tardiness, setuptimes, aofs, dataset='spread'):
    # earlines statistics
    mean_earliness = statistics.mean(earliness)
    std_earliness = statistics.stdev(earliness)
    # tardiness statistics
    mean_tardiness = statistics.mean(tardiness)
    std_tardiness = statistics.stdev(tardiness)
    # setuptime statistics
    mean_setuptimes = statistics.mean(setuptimes)
    std_setuptimes = statistics.stdev(setuptimes)
    # aof statistics
    mean_aofs = statistics.mean(aofs)
    std_aofs = statistics.stdev(aofs)
    # file writting
    result_string = 'Statistics for dataset {}: meanEarliness:{}, stdEarliness:{}, meanTardiness:{}, stdTardiness:{}, meanSetup:{}, stdSetup:{}, meanAOF:{}, stdAOF:{}'.format(dataset, mean_earliness, std_earliness, mean_tardiness, std_tardiness, mean_setuptimes, std_setuptimes, mean_aofs, std_aofs)
    #print(result_string)
    write_file(result_string, dataset=dataset)


def run_experiment_bayesian(prob, damping, init_state, dataset='large'):

    with open(f'../resources/datasets/{dataset}.pkl', 'rb') as file:
        setup_matrix, jobs, machines = pickle.load(file)

    # initiate arrays to compute mean and std of KPI's
    earliness_list = []
    tardiness_list = []
    setuptimes = []
    aofs = []

    save_figures_basefilename = f'prob{prob}-damping{damping}-dataset{dataset}'
    save_figures_basefilename = save_figures_basefilename.replace('.', ',')

    for run_index in range(num_repetitions):
        parameters = '{} - {}'.format(prob, damping)
        utilities, algo_result = run_brd(setup_matrix, jobs, machines, prob, damping)
        sum_tardiness = 0
        sum_earliness = 0
        sum_setup = 0
        for job in algo_result:
            earliness = max(0, job['due_date'] - job['end_date_production'])
            tardiness = max(0, job['end_date_production'] - job['due_date'])
            setup = job['end_date_setup'] - job['start_date']
            sum_tardiness += tardiness
            sum_earliness += earliness
            sum_setup += setup
        aof = sum_tardiness + sum_earliness + sum_setup

        earliness_list.append(sum_earliness)
        tardiness_list.append(sum_tardiness)
        setuptimes.append(sum_setup)
        aofs.append(aof)

       # result[parameters] = {'earliness': sum_earliness, 'tardiness': sum_tardiness,
      #                        'setup': sum_setup, 'aof': aof, 'iterations': len(utilities)}

        result_string = f'Earliness:{sum_earliness}, Tardiness:{sum_tardiness}, Setup:{sum_setup}, AOF:{aof} --> Prob:{prob}, Damping{damping}'
        print(result_string)
        write_file(result_string, dataset)

    # write computed statistics to file
    compute_statistics(earliness_list, tardiness_list, setuptimes, aofs, dataset=dataset)
    return np.mean(aofs)


################################### Bayes Opt ##########################

# relevant variables
random_state = 1
#save_intervall = 25
#repetitions = 700/save_intervall    # 700 controlls the number of target runs of the opt algorithm

def unpack(prob, damping):
    return prob, damping

# define an objective function
def objective_function(args):
    prob, damping = unpack(**args)
    # Annahmen: für operatoren aus vorherigen Tests (gegebenenfalls hinzufügen über hp.choice)
    return run_experiment_bayesian(prob, damping, None, dataset='urgency')

# define a search space
space = {
    'prob': hp.uniform('prob', 0.0, 0.97),
    'damping': hp.uniform('damping', 0.0, 0.99),
}


#run all iterations at once, without saving the states.
def run_bayesian_opt(evals, dataset):
    #keep track of progress via trials object (list of dicts)
    trials = Trials()

    # minimize the objective over the space
    best = fmin(objective_function, space, algo=tpe.suggest, max_evals=evals, trials=trials, rstate=np.random.RandomState(random_state))

    # print best
    print('best: ',best)

    #print whole progress
    #print(trials.trials)

    print('#####trials####')
    print(trials.best_trial)

    pickle.dump(trials, open(f"bayesOptBRDTrials{dataset}.p", "wb"))
    trials = pickle.load(open(f"bayesOptBRDTrials{dataset}.p", "rb"))

if __name__ == '__main__':
    spread = Process(target=run_bayesian_opt, args=(1800,"spread",))
    urgency = Process(target=run_bayesian_opt, args=(1800,"urgency",))

    spread.start()
    urgency.start()

    spread.join()
    urgency.join()
    #run_bayesian_opt(2000,'urgency')

#current best --- Prob:0.8404831648912066, Damping0.9781917179724587 --> meanAof 7432
